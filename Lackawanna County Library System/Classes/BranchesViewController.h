//
//  BranchesViewController.h
//  Brooklyn Public Library
//
//  Created by administrator on 10/3/17.
//  Copyright © 2017 Capira Technologies. All rights reserved.
//

#import "CMRViewController.h"
#import <MapKit/MapKit.h>
@interface BranchesViewController : CMRViewController <UITableViewDelegate, UITableViewDataSource>
    @property (weak, nonatomic) IBOutlet UITableView *tableBranches;
    @property (weak, nonatomic) IBOutlet MKMapView *mapView;
    - (IBAction)showBranchDirections:(id)sender;
    - (IBAction)showBranchPhone:(id)sender;

@end

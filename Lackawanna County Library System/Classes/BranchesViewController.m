//
//  BranchesViewController.m
//  Brooklyn Public Library
//
//  Created by administrator on 10/3/17.
//  Copyright © 2017 Capira Technologies. All rights reserved.
//

#import "BranchesViewController.h"
@import Firebase;

@interface BranchesViewController ()
    {
        NSMutableArray *listOfBranches;
        NSMutableArray *listOfExpandedCells;
    }
@end

@implementation BranchesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableBranches.dataSource= self;
    self.tableBranches.delegate = self;
    
    self.tableBranches.separatorInset = UIEdgeInsetsZero;
    self.tableBranches.layoutMargins = UIEdgeInsetsZero;
    
    listOfBranches = [NSMutableArray new];
    listOfExpandedCells = [NSMutableArray new];

    
    NSDictionary *branchNames = [self getBranchData];
    NSLog(@"BRANCH NAMES %@", branchNames);
   // NSDictionary *branchList = [branchNames objectAtIndex:0];
    NSArray*keys=[branchNames allKeys];

    for(int i=0; i<[branchNames count]; i++){
        NSDictionary *branchData = [branchNames objectForKey:keys[i]];

        CTLibraryBranch *branch = [CTLibraryBranch new];
        branch.name         = [branchData objectForKey:@"name"];
        branch.address      = [[branchData objectForKey:@"address"] stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
        branch.phone        = [branchData objectForKey:@"phone"];
        branch.latitude     = [branchData objectForKey:@"latitude"];
        branch.longitude    = [branchData objectForKey:@"longitude"];
        branch.bus          = [branchData objectForKey:@"bus"];
        branch.subway       = [branchData objectForKey:@"subway"];
        branch.monday       = [branchData objectForKey:@"Monday"];
        branch.tuesday      = [branchData objectForKey:@"Tuesday"];
        branch.wednesday    = [branchData objectForKey:@"Wednesday"];
        branch.thursday     = [branchData objectForKey:@"Thursday"];
        branch.friday       = [branchData objectForKey:@"Friday"];
        branch.saturday     = [branchData objectForKey:@"Saturday"];
        branch.sunday       = [branchData objectForKey:@"Sunday"];
       // branch.hours        = [branchData objectForKey:@"hours"];
        branch.website       = [branchData objectForKey:@"url"];
        branch.facebook       = [branchData objectForKey:@"facebook"];
        branch.instagram       = [branchData objectForKey:@"instagram"];
        branch.twitter       = [branchData objectForKey:@"twitter"];

        
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"EEEE"];
        // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
        NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    
        branch.hours = [NSString stringWithFormat:@"Today's Hours: %@",[branchData objectForKey: [dateFormatter stringFromDate:[NSDate date]]]];
        if([[branchData objectForKey: [dateFormatter stringFromDate:[NSDate date]]] length ] == 0){
            branch.hours = @"";
        }
        
       //Convert blank days to "Closed"
        [branch cleanValuesForWeekdays];
       
        [listOfBranches addObject:branch];
    }
    
    NSLog(@"listOfBranches %@", listOfBranches);
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    [listOfBranches sortUsingDescriptors:[NSArray arrayWithObject:sort]];
    
    [self.tableBranches reloadData];
    self.topNavBar.topItem.title = @"Locations";
    
    for (CTLibraryBranch *branch in listOfBranches)
    {
       // NSArray *tokens = [branch.position componentsSeparatedByString:@", "];
        NSLog(@"LAT LONG %@ %@", branch.latitude, branch.longitude);
        CLLocationCoordinate2D centerCoordinate = CLLocationCoordinate2DMake([branch.latitude doubleValue], [branch.longitude doubleValue]);
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        [annotation setCoordinate:centerCoordinate];
        [annotation setTitle:branch.name];
        [self.mapView addAnnotation:annotation];
    }
    MKCoordinateRegion mapRegion;
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = 41.650433;
    coordinate.longitude = -70.245142;
    mapRegion.center = coordinate;
    mapRegion.span.latitudeDelta = 0.5;
    mapRegion.span.longitudeDelta = 0.5;
    
    [self.mapView setZoomEnabled:true];
    [self.mapView showAnnotations:_mapView.annotations animated:NO];
    [self.mapView setRegion:mapRegion animated: YES];

}

- (void)mapView:(MKMapView *)mapView
didSelectAnnotationView:(MKAnnotationView *)view
{
    MKPointAnnotation *annotation = view.annotation;
    CTLibraryBranch *chosenBranch;
    int i = 0;
    for (CTLibraryBranch *branch in listOfBranches)
    {
        if ([branch.name isEqualToString:annotation.title])
        {
            chosenBranch = branch;
            break;
        }
        i++;
    }

    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
    
    [listOfExpandedCells addObject:indexPath];
    
    [_tableBranches scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    [self.tableBranches reloadData];
    NSLog(@"%@", chosenBranch.name);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSDictionary *)getBranchData
{
    NSArray *paths;
    NSString *documentsDirectory;
    NSString *filePath;
    
    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    documentsDirectory = [paths objectAtIndex:0];
    
    //filename that will save in device
    filePath = [documentsDirectory stringByAppendingPathComponent:@"locations.json"];
    
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:filePath])
    {
        // if failed to get the json server uses the local built
        NSLog(@"File does not exist");
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"locations" ofType:@"json"];
        NSData *data = [NSData dataWithContentsOfFile:filePath];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        return json;
    }
    
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    return json;
}

- (IBAction)showBranchDirections:(id)sender
{
     CGPoint hitPoint = [sender convertPoint:CGPointZero toView:self.tableBranches];
    NSIndexPath *hitIndex = [self.tableBranches indexPathForRowAtPoint:hitPoint];
    
    CTLibraryBranch *temp = [listOfBranches objectAtIndex:hitIndex.row];
    
    //GA Hit
   
   
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"comgooglemaps://"]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?daddr=%@&directionsmode=driving", [temp.address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]]];
    }else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.apple.com?daddr=%@", [temp.address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]]];
    }
}


- (IBAction)showbranchWebsite:(id)sender
{
     CGPoint hitPoint = [sender convertPoint:CGPointZero toView:self.tableBranches];
    NSIndexPath *hitIndex = [self.tableBranches indexPathForRowAtPoint:hitPoint];
    
    CTLibraryBranch *temp = [listOfBranches objectAtIndex:hitIndex.row];
    
    
    if (temp.website.length > 0){
        [self displayBrowser: temp.website];
    }else {
           UIAlertView *alertNoDialer = [[UIAlertView alloc] initWithTitle:@"No Website Available"
                                                                   message:@"The branch website requested does not exist."
                                                                  delegate:self
                                                         cancelButtonTitle:@"Close"
                                                         otherButtonTitles:nil];
           [alertNoDialer show];
           
    }
    

}

- (IBAction)showBranchPhone:(id)sender
{
    CGPoint hitPoint = [sender convertPoint:CGPointZero toView:self.tableBranches];
    NSIndexPath *hitIndex = [self.tableBranches indexPathForRowAtPoint:hitPoint];
    
    CTLibraryBranch *temp = [listOfBranches objectAtIndex:hitIndex.row];
    
    //GA Hit
   
    
    NSString *phoneNumber = [NSString stringWithFormat:@"tel://%@", [[temp.phone componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""]];
   
    
    NSLog(@"Phone Number: %@", phoneNumber);
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString: phoneNumber]]){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: phoneNumber]];
    }else {
        
        UIAlertView *alertNoDialer = [[UIAlertView alloc] initWithTitle:@"Cannot Dial Number"
                                                                message:@"This Apple device does not support making phone calls."
                                                               delegate:self
                                                      cancelButtonTitle:@"Close"
                                                      otherButtonTitles:nil];
        [alertNoDialer show];
        
    }
}


- (IBAction)showBranchSocialMedia:(id)sender
{
    CGPoint hitPoint = [sender convertPoint:CGPointZero toView:self.tableBranches];
    NSIndexPath *hitIndex = [self.tableBranches indexPathForRowAtPoint:hitPoint];
    
    CTLibraryBranch *temp = [listOfBranches objectAtIndex:hitIndex.row];
    
    
    
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Social Media"
                                          message:@"Which account would you like to view"
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
        
    }];
    if(temp.facebook.length){
        UIAlertAction *adultAction = [UIAlertAction
                                      actionWithTitle:@"Facebook"
                                      style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction *action)
                                      {
            
            [FIRAnalytics logEventWithName:@"user_interaction"
                                parameters:@{
                                    @"name": @"Facebook"
                                }];
            [self displayBrowser: temp.facebook];
            
            
        }];
        [alertController addAction:adultAction];
        
    }
    if(temp.twitter.length){
        UIAlertAction *teenAction = [UIAlertAction
                                     actionWithTitle:@"Twitter"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction *action)
                                     {
            
            [FIRAnalytics logEventWithName:@"user_interaction"
                                parameters:@{
                                    @"name": @"Twitter"
                                }];
            [self displayBrowser: temp.twitter];
            
            
        }];
        [alertController addAction:teenAction];
        
    }
    if(temp.instagram.length){
        UIAlertAction *largePrint = [UIAlertAction
                                     actionWithTitle:@"Instagram"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction *action)
                                     {
            
            [FIRAnalytics logEventWithName:@"user_interaction"
                                parameters:@{
                                    @"name": @"Instagram"
                                    
                                }];
            [self displayBrowser: temp.instagram];
            
            
        }];
        [alertController addAction:largePrint];
        
    }
    
    if(!temp.instagram.length && !temp.facebook.length && !temp.twitter.length){
        UIAlertView *alertNoDialer = [[UIAlertView alloc] initWithTitle:@"No Social Media Accounts"
                                                                       message:@"This library has no social media accounts."
                                                                      delegate:self
                                                             cancelButtonTitle:@"Close"
                                                             otherButtonTitles:nil];
               [alertNoDialer show];
        return;
    }
    
    [alertController addAction:cancelAction];
    
    UIPopoverPresentationController *popover = alertController.popoverPresentationController;
    if (popover)
    {
        popover.sourceView = sender;
        // popover.sourceRect = sender.bounds;
        popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
    }
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma  mark UITABLEVIEW
    
- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([listOfExpandedCells containsObject:indexPath])
    {
        [listOfExpandedCells removeObject:indexPath];
    }
    else
    {
        [listOfExpandedCells addObject:indexPath];
    }
    
    [self.tableBranches reloadData];
    
}
    
- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    
}
    
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
    {
        // Return the number of sections.
        return 1;
    }
    
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
    {
        // Return the number of rows in the section.
        NSLog(@"number %lu", (unsigned long)[listOfBranches count]);
        return [listOfBranches count];
    }
    
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
    {
        if ([listOfExpandedCells containsObject:indexPath])
        {
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                return 570;
            return 488;
        }
        else
        {
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                return 64;
            return 40;
        }
    }
    
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
    {
        
        static NSString *CellIdentifier = @"libraryCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        // Configure the cell...
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        
        UILabel *lblBranchName = (UILabel *)[cell viewWithTag:100];
        UILabel *lblBranchAddress = (UILabel *)[cell viewWithTag:101];
        UILabel *lblBranchPhone = (UILabel *)[cell viewWithTag:102];
        UILabel *lblBranchAccessible = (UILabel *)[cell viewWithTag:103];
        UILabel *lblBranchOpenToday = (UILabel *)[cell viewWithTag:104];
        UILabel *lblBranchHours = (UILabel *)[cell viewWithTag:105];
        
        UIImageView *imgArrow = (UIImageView *)[cell viewWithTag:200];
        UIImageView *imgClosure = (UIImageView *)[cell viewWithTag:201];
        UIImageView *imgAccessibility = (UIImageView *)[cell viewWithTag:202];
        
    
        
        
        if ([listOfExpandedCells containsObject:indexPath])
        {
            imgArrow.image = [UIImage imageNamed:@"tablet_arrow_up.png"];
        }else{
            imgArrow.image = [UIImage imageNamed:@"tablet_arrow_down.png"];
        }
        
        CTLibraryBranch *branch = [listOfBranches objectAtIndex:indexPath.row];
        
        if ([branch.hours containsString:@"Temporary"])
        {
            imgClosure.hidden = NO;
        }else{
            imgClosure.hidden = YES;
        }

       
        lblBranchName.text = [NSString stringWithFormat:@"%@", branch.name];;
        lblBranchPhone.text = branch.phone;
        lblBranchAddress.text = branch.address;
        lblBranchAccessible.text = branch.access;
        lblBranchOpenToday.text = branch.hours;
        
        NSString *hoursString = [NSString stringWithFormat:@"Monday: %@\nTuesday: %@\nWednesday: %@\nThursday: %@\nFriday: %@\nSaturday: %@\nSunday: %@", branch.monday, branch.tuesday, branch.wednesday, branch.thursday, branch.friday, branch.saturday, branch.sunday];
        
        lblBranchHours.text = hoursString;
        
        // Fix for iOS 7 to clear backgroundColor
        cell.backgroundColor = [UIColor clearColor];
        cell.backgroundView = [UIView new];
        cell.selectedBackgroundView = [UIView new];
        
        [self.tableBranches setBackgroundView:nil];
        [self.tableBranches setBackgroundColor:[UIColor clearColor]];
        
        return cell;
        
    }


@end

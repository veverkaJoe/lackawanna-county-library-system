//
//  CTFormHelper.h
//  CTFormsHelper
//
//  Created by Michael Berse on 11/22/16.
//  Copyright © 2016 Capira Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FormHelperDelegate <NSObject>
@required
- (void) formSubmissionCompleted:(BOOL)success httpStatusCode:(NSInteger)code responseData:(NSData *)response;
@end

typedef NS_ENUM(NSInteger, HTTP_METHOD)
{
    POST = 1,
    GET
};

@interface CTFormHelper : NSObject


@property (nonatomic, strong)   NSDictionary    *formFieldsDictionary;
@property (nonatomic)           HTTP_METHOD     method;
@property (nonatomic, strong)   NSString        *formURLString;
@property (nonatomic, strong)   id              delegate;


- (void)submitForm;
- (void)submitFormAsJSON;

@end

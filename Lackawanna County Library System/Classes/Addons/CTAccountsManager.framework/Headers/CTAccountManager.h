//
//  CTAccountManager.h
//  CTAccountsManager
//
//  Created by Michael Berse on 11/19/16.
//  Copyright © 2016 Capira Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTPatronAccount.h"
#import "CTCheckoutItem.h"
#import "CTHoldItem.h"
#import "CTFinesItem.h"
#import "CTBlockMessage.h"

typedef NS_ENUM(NSUInteger, AccountItemAction){
    kAccountItemActionRenew,
    kAccountItemActionCancel,
    kAccountItemActionFreeze,
    kAccountItemActionUnfreeze
};

typedef NS_ENUM(NSUInteger, CirculationAction){
    kCirculationActionCheckout,
    kCirculationActionCheckin
};

@protocol CTAccountsManagerDelegate <NSObject>


@optional
- (void)accountInvalidated:(CTPatronAccount *)account;
- (void)accountValidated:(CTPatronAccount *)account;
- (void)validationFailed:(NSString *)failureMsg;
- (void)didFailWithError:(NSString *)errorMsg httpStatusCode: (NSInteger)statusCode;
- (void)didReceiveFines:(NSArray *)fineItems forAccount:(CTPatronAccount *)account;
- (void)didReceiveCheckouts:(NSArray *)checkoutItems forAccount:(CTPatronAccount *)account;
- (void)didReceiveDigitalCheckouts:(NSArray *)checkoutItems forAccount:(CTPatronAccount *)account;
- (void)didReceiveHolds:(NSArray *)holdItems forAccount:(CTPatronAccount *)account;
- (void)didReceiveReadingHistory:(NSArray *)readingHistoryItems forAccount:(CTPatronAccount *)account;
- (void)didReceiveBlocksMessages:(NSArray *)blocksMessagesItems forAccount:(CTPatronAccount *)account;
- (void)didReceiveDigitalHolds:(NSArray *)holdItems forAccount:(CTPatronAccount *)account;
- (void)itemActionSuccessful:(BOOL)success responseMessage:(NSString *)message actionType:(AccountItemAction)action;
- (void)itemRequestSuccessful:(BOOL)success responseMessage:(NSString *)message forAccount:(CTPatronAccount *)account;

- (void)circulationRequestSuccessful:(BOOL)success
                     responseMessage:(NSString *)message
                     circulationType:(CirculationAction)action
                      originalItemId:(NSString *)itemId
                           itemTitle:(NSString *)title
                             dueDate:(NSString *)dueDateVal;
@end

@interface CTAccountManager : NSObject

@property (nonatomic) id delegate;

+ (id)sharedInstance;

- (void)setAPIVersion:(NSString *)version libraryCode:(NSString *)code;
- (void)validateAllAccounts;
- (void)validateCredentials:(NSString *)barcode password:(NSString *)pass;
- (void)updatedStoredAccounts;
- (void)updateStoredAccountInformation:(BOOL)deleteInvalid;
- (NSArray *)getAllStoredAccounts;
- (NSInteger)numberOfAccounts;
- (void)removeAllAccounts;
- (void)addAccount:(CTPatronAccount *)account;
- (void)removeAccount:(CTPatronAccount *)account;
- (void)removeAccountWithBarcode:(NSString *)barcode;
- (void)setPrimaryAccount:(NSString *)barcode;
- (BOOL)accountExistsWithBarcode:(NSString *)barcode;
- (CTPatronAccount *)getAccountWithBarcode:(NSString *)barcode;
- (CTPatronAccount *)getPrimaryAccount;
- (void)getCheckouts:(CTPatronAccount *)account;
- (void)getHolds:(CTPatronAccount *)account;
- (void)getFines:(CTPatronAccount *)account;
- (void)getReadingHistory:(CTPatronAccount *)account;
- (void)getBlocksMessages:(CTPatronAccount *)account;
- (void)checkoutItem:(NSString *)itemId forAccount:(CTPatronAccount *)account;
- (void)checkinItem:(NSString *)itemId forAccount:(CTPatronAccount *)account;
- (void)requestItem:(NSString *)recordId forAccount:(CTPatronAccount *)account pickupLocation:(NSString *)pickupCode volume:(NSString *)volumeId;
- (void)manageItems:(NSArray *)recordIds forAccount:(CTPatronAccount *)account actionType:(AccountItemAction)itemAction;
- (void)suspendItems:(NSArray *)recordIds forAccount:(CTPatronAccount *)account startDate:(NSDate*)startingDate untilDate:(NSDate *)endingDate;

//Temp
- (void)getDigitalCheckouts:(CTPatronAccount *)account;
- (void)getDigitalHolds:(CTPatronAccount *)account;


@end

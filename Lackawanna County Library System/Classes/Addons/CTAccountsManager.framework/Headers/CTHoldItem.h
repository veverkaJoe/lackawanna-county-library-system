//
//  CTHoldItem.h
//  CTAccountsManager
//
//  Created by Michael Berse on 2/6/17.
//  Copyright © 2017 Capira Technologies. All rights reserved.
//

#import "CTAccountItem.h"

@interface CTHoldItem : CTAccountItem

@property (nonatomic, strong) NSString *pickupLocation, *freezeId;

@property (nonatomic) BOOL isCancellable,
                            isFrozen,
                            isFreezable,
                            isReady;

@end

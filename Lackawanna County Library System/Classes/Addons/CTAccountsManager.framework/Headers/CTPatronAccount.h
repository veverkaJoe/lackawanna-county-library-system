//
//  CTPatronAccount.h
//  CTAccountsManager
//
//  Created by Michael Berse on 11/19/16.
//  Copyright © 2016 Capira Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

#define KEY_address1                    @"address1"
#define KEY_address2                    @"address2"
#define KEY_barcode                     @"barcode"
#define KEY_recordId                    @"recordId"
#define KEY_name                        @"name"
#define KEY_username                    @"username"
#define KEY_emailAddress                @"emailAddress"
#define KEY_expirationDate              @"expirationDate"
#define KEY_expirationFormattedDate     @"expirationFormattedDate"
#define KEY_homeLibrary                 @"homeLibrary"
#define KEY_pCode1                      @"pCode1"
#define KEY_pCode2                      @"pCode2"
#define KEY_pCode3                      @"pCode3"
#define KEY_totalCheckouts              @"totalCheckouts"
#define KEY_totalRenewals               @"totalRenewals"
#define KEY_createdDate                 @"createdDate"
#define KEY_updatedDate                 @"updatedDate"
#define KEY_languagePreference          @"languagePreference"
#define KEY_noticePreference            @"noticePreference"
#define KEY_telephone1                  @"telephone1"
#define KEY_telephone2                  @"telephone2"
#define KEY_password                    @"password"
#define KEY_pickupLibrary               @"pickupLibrary"
#define KEY_pType                       @"pType"
#define KEY_hasFines                    @"hasFines"
#define KEY_isBlocked                   @"isBlocked"
#define KEY_isExpired                   @"isExpired"
#define KEY_isLinked                    @"isLinked"
#define KEY_isPrimaryAccount            @"isPrimaryAccount"

@interface CTPatronAccount : NSObject

@property (nonatomic, strong) NSString *address1,
                                        *address2,
                                        *barcode,
                                        *recordId,
                                        *name,
                                        *username,
                                        *emailAddress,
                                        *expirationDate,
                                        *expirationFormattedDate,
                                        *homeLibrary,
                                        *pCode1,
                                        *pCode2,
                                        *pCode3,
                                        *pType,
                                        *createdDate,
                                        *updatedDate,
                                        *languagePreference,
                                        *noticePreference,
                                        *telephone1,
                                        *telephone2,
                                        *password,
                                        *pickupLibrary;

@property (nonatomic) NSInteger totalCheckouts,
                                totalRenewals;

@property (nonatomic) double    totalFines;

@property (nonatomic) BOOL hasFines,
                           isBlocked,
                           isExpired,
                           isLinked,
                           isPrimaryAccount;



@end

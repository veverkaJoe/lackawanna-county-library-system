//
//  CTAccountItem.h
//  CTAccountsManager
//
//  Created by Michael Berse on 2/6/17.
//  Copyright © 2017 Capira Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CTAccountItem : NSObject

@property (nonatomic, strong) NSString *recordId,
                                *title,
                                *status,
                                *homeLocation;

@property (nonatomic) BOOL isDigital;
@property (nonatomic, strong) NSString *vendorName;
@end

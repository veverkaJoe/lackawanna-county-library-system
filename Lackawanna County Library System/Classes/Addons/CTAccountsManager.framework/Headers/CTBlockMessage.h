//
//  CTBlockMessage.h
//  CTAccountsManager
//
//  Created by Michael on 1/6/19.
//  Copyright © 2019 Capira Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface CTBlockMessage : NSObject

@property (nonatomic, strong) NSString *blockStatus;
@property (nonatomic, strong) NSString *blockType;
@property (nonatomic, strong) NSString *blockDescription;
@property (nonatomic, strong) NSString *blockValue;
@end

 

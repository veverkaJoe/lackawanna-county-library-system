//
//  CTFinesItem.h
//  CTAccountsManager
//
//  Created by Michael Berse on 8/1/17.
//  Copyright © 2017 Capira Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CTFinesItem : NSObject

@property (nonatomic, strong) NSString *fineDescription, *fineAmount;

@end

//
//  CTCheckoutItem.h
//  CTAccountsManager
//
//  Created by Michael Berse on 2/6/17.
//  Copyright © 2017 Capira Technologies. All rights reserved.
//

#import "CTAccountItem.h"

@interface CTCheckoutItem : CTAccountItem

@property (nonatomic, strong) NSString *dueDate;
@property (nonatomic, strong) NSString *checkoutDate;
@property (nonatomic) BOOL isOverdue;


@end

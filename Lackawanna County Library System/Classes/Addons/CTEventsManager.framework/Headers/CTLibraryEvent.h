//
//  CTEvent.h
//  CTEventsManager
//
//  Created by Michael Berse on 11/23/16.
//  Copyright © 2016 Capira Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EventKit/EventKit.h>
@interface CTLibraryEvent : NSObject

@property (nonatomic, strong) NSString *title,
                                        *date,
                                        *time,
                                        *summary,
                                        *location,
                                        *library,
                                        *link,
                                        *recordId,
                                        *length,
                                        *type,
                                        *agegroup,
                                        *presenter,
                                        *contactPhone,
                                        *contactEmail,
                                        *contactName,
                                        *imageURL,
                                        *iCalStart,
                                        *iCalEnd;


@property (nonatomic) BOOL isFeatured,
                            allowsSignup,
                            allowsICal;

@property (nonatomic) NSInteger alarmHoursInPast;

- (BOOL)canAddEventToCalendar;
- (BOOL)addEventToCalendar;


@end

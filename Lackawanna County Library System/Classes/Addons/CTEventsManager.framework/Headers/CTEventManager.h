//
//  CTEventManager.h
//  CTEventsManager
//
//  Created by Michael Berse on 11/24/16.
//  Copyright © 2016 Capira Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

enum {
    AlarmNotice_1Hr = 0,  //1 Hour
    AlarmNotice_4Hr = 1,  //4 Hours
    AlarmNotice_24Hr = 2, //1 Day
    AlarmNotice_48Hr = 3, //2 Day
    AlarmNotice_72Hr = 4, //3 Day
    AlarmNotice_168Hr = 5 //1 Week
};

typedef NSInteger CTEventAlarmNotice;

@protocol CTEventsManagerDelegate <NSObject>

@required

@optional
- (void) didSaveEventPreferences:(BOOL)successful;
- (void) didReceiveEvents:(NSArray *)events;
- (void) didReceiveNotificationPreferences:(NSArray *)preferences;
- (void) didFailWithError:(NSString *)errorMsg httpStatusCode: (NSInteger)statusCode;
@end

@interface CTEventManager : NSObject

@property (nonatomic) id delegate;

- (id)initWithVersion:(NSString *)version libraryCode:(NSString *)code;
- (void)executeWithQuery:(NSString *)query;
- (void)executeWithQuery:(NSString *)query library:(NSString *)name location:(NSString *)room;
- (void)executeWithQuery:(NSString *)query customParameters:(NSDictionary *)parameters;
- (void)retrieveEventNotificationPreferences:(NSString *)userBarcode;
- (void)saveEventNotificationPreferences:(NSString *)userBarcode preferences:(NSArray *)userPreferences;
- (void)showOnlyFeaturedEvents:(BOOL)value;
- (void)setAlarmNoticePeriod:(CTEventAlarmNotice)noticeLength;
- (NSArray *)getCachedEvents;


@end

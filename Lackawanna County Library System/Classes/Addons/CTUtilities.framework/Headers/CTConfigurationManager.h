//
//  CTConfigurationManager.h
//  CTUtilities
//
//  Created by Michael Berse on 11/29/16.
//  Copyright © 2016 Capira Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CTConfigurationManager : NSObject

+ (NSDictionary *)JSONConfigurationFileAsDictionary:(NSString *)configFileName;
+ (NSArray *)JSONConfigurationFileAsArray:(NSString *)configFileName;
+ (void)downloadConfiguration:(NSString *)configFileName fromURLString:(NSString *)URL;
+ (void)setDeviceToken:(NSString *)token;
+ (void)setDeviceName:(NSString *)name;
+ (NSString *)getDeviceToken;
+ (NSString *)getDeviceName;
+ (BOOL)deviceDefaultsConfigured;
+ (void)initializeDefaults;
+ (void)registerDeviceWithServerAPIVersion:(NSString *)apiVersion libraryCode:(NSString *)code;
@end

//
//  CTUtilities.h
//  CTUtilities
//
//  Created by Michael Berse on 11/29/16.
//  Copyright © 2016 Capira Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CTUtilities.
FOUNDATION_EXPORT double CTUtilitiesVersionNumber;

//! Project version string for CTUtilities.
FOUNDATION_EXPORT const unsigned char CTUtilitiesVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CTUtilities/PublicHeader.h>
@interface CTUtilities : NSObject


+ (void)registerDeviceWithServer:(NSString *)endpointURL httpParameters:(NSDictionary *)paramsDict deviceToken:(NSData *)token;
+ (NSString *)currentDeviceName;
+ (NSString *)generateRandomString:(NSInteger)length;
+ (NSString*)encodeURLString:(NSString *)string;
@end



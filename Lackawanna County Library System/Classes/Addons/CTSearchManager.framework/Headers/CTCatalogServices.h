//
//  CTCatalogSearchService.h
//  CTSearchManager
//
//  Created by Michael Berse on 2/10/17.
//  Copyright © 2017 Capira Technologies, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTSearchResult.h"

@protocol CTCatalogServicesDelegate <NSObject>
@optional
- (void)didReceiveResults:(NSArray *)searchResults;
- (void)didReceiveLookup:(BOOL)success lookupResult:(CTSearchResult *)result;
- (void)didReceiveItemDetails:(NSString *)displayHTML;
@required
- (void)didFailWithError:(NSString *)errorMsg httpStatusCode: (NSInteger)statusCode;
@end

@interface CTCatalogServices : NSObject

@property (nonatomic) id delegate;

- (id)initWithVersion:(NSString *)version libraryCode:(NSString *)code;

- (void)executeSearchWithQuery:(NSString *)query;

- (void)executeSearchWithQuery:(NSString *)query
            materialCode:(NSString *)code
            libraryScope:(NSString *)scope;

- (void)executeSearchWithQuery:(NSString *)query
        customParameters:(NSDictionary *)parameters;

- (void)retrieveItemDetails:(NSString *)recordId;

- (void)executeISBNLookup:(NSString *)isbn;

@end

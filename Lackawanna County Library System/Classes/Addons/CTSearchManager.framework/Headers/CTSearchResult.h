//
//  CTSearchResult.h
//  CTSearchManager
//
//  Created by Michael Berse on 2/10/17.
//  Copyright © 2017 Capira Technologies, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CTSearchResult : NSObject

@property (nonatomic, strong) NSString *title,
                                        *recordId,
                                        *author,
                                        *material,
                                        *publisher,
                                        *location,
                                        *callNumber,
                                        *coverImage,
                                        *reserveId,
                                        *link,
                                        *isbn,
                                        *upc,
                                        *edition;

@property (nonatomic, strong) NSArray *volumes;

@property (nonatomic) BOOL isRequestable, isDigital;

@end

//
//  CTSearchManager.h
//  CTSearchManager
//
//  Created by Michael Berse on 2/10/17.
//  Copyright © 2017 Capira Technologies, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CTSearchManager.
FOUNDATION_EXPORT double CTSearchManagerVersionNumber;

//! Project version string for CTSearchManager.
FOUNDATION_EXPORT const unsigned char CTSearchManagerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CTSearchManager/PublicHeader.h>
#import "CTSearchResult.h"
#import "CTCatalogServices.h"


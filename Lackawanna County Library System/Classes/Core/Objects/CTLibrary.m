//
//  CTLibrary.m
//  KCKPL
//
//  Created by Michael Berse on 12/3/16.
//  Copyright © 2016 Michael Berse. All rights reserved.
//

#import "CTLibrary.h"

@implementation CTLibrary

- (id)initWithDict:(NSDictionary *)dict
{
    if (self = [super init])
    {
        self.name               = [dict objectForKey:@"name"];
        self.code               = [dict objectForKey:@"code"];
        self.website            = [dict objectForKey:@"website"];
        self.phones             = [dict objectForKey:@"phones"];
        self.emails             = [dict objectForKey:@"emails"];
        self.addresses          = [dict objectForKey:@"addresses"];
        self.smsNumber          = [[dict objectForKey:@"sms"] objectForKey:@"number"];
        self.smsTextCode        = [[dict objectForKey:@"sms"] objectForKey:@"textCode"];
        self.customWebsites     = [dict objectForKey:@"customWebsites"];
        self.socialMedia        = [dict objectForKey:@"socialMedia"];
        self.libraryCardSettings  = [dict objectForKey:@"libraryCardSettings"];
        self.libraryBranches    = [dict objectForKey:@"branches"];

    }
    
    return self;
}

- (BOOL)SMSEnabled
{
    return ([self.smsNumber length] != 0);
}


- (NSArray *)getBranches{
    NSArray *sortedArray = [[self.libraryBranches allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    return sortedArray;
}


@end

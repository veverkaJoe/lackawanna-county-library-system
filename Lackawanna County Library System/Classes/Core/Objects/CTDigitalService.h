//
//  CTDigitalService.h
//  CapiraMobile Ready
//
//  Created by Michael on 5/8/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CTDigitalService : NSObject
@property (nonatomic, strong) NSString *serviceName;
@property (nonatomic, strong) NSString *serviceURL;
@property (nonatomic, strong) NSString *serviceSelector;
@property (nonatomic, strong) NSString *serviceImage;
@property (nonatomic, strong) NSString *serviceDescription;
@end

NS_ASSUME_NONNULL_END

//
//  CTLibrary.h
//  CapiraMobile
//
//  Created by Michael Berse on 3/3/20.
//  Copyright © 2020 Michael Berse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTLibraryBranch.h"

@interface CTLibrary : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *website;
@property (nonatomic) NSDictionary *phones;
@property (nonatomic) NSDictionary *addresses;
@property (nonatomic) NSDictionary *emails;
@property (nonatomic) NSDictionary *libraryCardSettings;
@property (nonatomic, strong) NSString *smsNumber;
@property (nonatomic, strong) NSString *smsTextCode;
@property (nonatomic) NSDictionary *customWebsites;
@property (nonatomic) NSDictionary *socialMedia;
@property (nonatomic) NSMutableDictionary *libraryBranches;

@property (nonatomic) BOOL requiresPassword;
@property (nonatomic) BOOL suspensionDisabled;

- (BOOL)SMSEnabled;

- (id)initWithDict:(NSDictionary *) dict;
- (NSArray *)getBranches;


@end

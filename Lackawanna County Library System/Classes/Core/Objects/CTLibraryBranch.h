//
//  CTLibraryBranch.h
//  Monmouth County Library
//
//  Created by Capira on 3/7/17.
//  Copyright © 2017 Capira Technologies LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CTLibraryBranch : NSObject



@property (nonatomic) NSString *name;
@property (nonatomic) NSString *phone;
@property (nonatomic) NSString *address;
@property (nonatomic) NSString *latitude;
@property (nonatomic) NSString *longitude;
@property (nonatomic) NSString *bus;
@property (nonatomic) NSString *subway;
@property (nonatomic) NSString *monday;
@property (nonatomic) NSString *tuesday;
@property (nonatomic) NSString *wednesday;
@property (nonatomic) NSString *thursday;
@property (nonatomic) NSString *friday;
@property (nonatomic) NSString *saturday;
@property (nonatomic) NSString *sunday;
@property (nonatomic) NSString *hours;
@property (nonatomic) NSString *access;
@property (nonatomic) NSString *path;
@property (nonatomic) NSString *website;
@property (nonatomic) NSString *facebook;
@property (nonatomic) NSString *twitter;
@property (nonatomic) NSString *instagram;



- (void)cleanValuesForWeekdays;



@end

//
//  CTLibraryBranch.m
//   County Library
//
//  Created by Capira on 3/7/17.
//  Copyright © 2017 Capira Technologies LLC. All rights reserved.
//

#import "CTLibraryBranch.h"

@implementation CTLibraryBranch

- (void)cleanValuesForWeekdays
{

    if ([_monday length] == 0)
    {
        _monday = @"Closed";
    }
    
    if ([_tuesday length] == 0)
    {
        _tuesday = @"Closed";
    }
    
    if ([_wednesday length] == 0)
    {
        _wednesday = @"Closed";
    }
    
    if ([_thursday length] == 0)
    {
        _thursday = @"Closed";
    }
    
    if ([_friday length] == 0)
    {
        _friday = @"Closed";
    }
    
    if ([_saturday length] == 0)
    {
        _saturday = @"Closed";
    }
    
    if ([_sunday length] == 0)
    {
        _sunday = @"Closed";
    }
}

@end

//
//  AppDelegate.m
//  CapiraMobile Ready
//
//  Created by Michael on 3/14/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import "AppDelegate.h"
#import <CTUtilities/CTConfigurationManager.h>
#import <CTAccountsManager/CTAccountsManager.h>
@import Firebase;
//@import Firebase;

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface AppDelegate ()
{
    NSString *libraryCode;
    NSString *configUrl;
    CTAccountManager *accountManager;

}
@end

@implementation AppDelegate

NSString *const kGCMMessageIDKey = @"gcm.message_id";


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    //Get our running code for the library
 
    libraryCode = [[CTConfigurationManager JSONConfigurationFileAsDictionary:@"config.json"] objectForKey:@"code"];
    configUrl   = [NSString stringWithFormat:@"https://webservices.capiramobile.com/configurations/capiraready/%@/", libraryCode];
    
    NSLog(@"Config Download URL: %@" ,configUrl);
    
    //Download configuration files
//    [CTConfigurationManager downloadConfiguration:@"config.json"   fromURLString:configUrl];
    
    //Push Notifications
    if ([UNUserNotificationCenter class] != nil) {
       // iOS 10 or later
       [UNUserNotificationCenter currentNotificationCenter].delegate = self;
       UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
           UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
       [[UNUserNotificationCenter currentNotificationCenter]
           requestAuthorizationWithOptions:authOptions
           completionHandler:^(BOOL granted, NSError * _Nullable error) {}];
     }

     [application registerForRemoteNotifications];

     //Configure Firebase Messaging and Analytics
//     [FIRApp configure];
     [FIRMessaging messaging].delegate = self;
     

     [[FIRInstanceID instanceID] instanceIDWithHandler:^(FIRInstanceIDResult * _Nullable result,
                                                         NSError * _Nullable error) {
       if (error != nil) {
         NSLog(@"Error fetching remote instance ID: %@", error);
       } else if (result.token.length > 0) {
             NSLog(@"FCM Token: %@", result.token);
             if (![CTConfigurationManager deviceDefaultsConfigured])
             {
                 [CTConfigurationManager initializeDefaults];
             }
             
            [CTConfigurationManager setDeviceToken:result.token];
           [CTConfigurationManager registerDeviceWithServerAPIVersion:@"v1" libraryCode:self->libraryCode];
      }
     }];
    
    //Updated local accounts
    [self updateLocalAccounts];
    
    UIStoryboard *storyboard;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:[NSBundle mainBundle]];
    }else{
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:[NSBundle mainBundle]];
    }
    
    self.window.rootViewController = [storyboard instantiateInitialViewController];
    [self.window makeKeyAndVisible];
    
    return YES;
}

#pragma mark -
#pragma mark PUSH

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"FCM Token: %@", fcmToken);
    
    // Notify about received token.
    NSDictionary *dataDict = [NSDictionary dictionaryWithObject:fcmToken forKey:@"token"];
    [[NSNotificationCenter defaultCenter] postNotificationName:
     @"FCMToken" object:nil userInfo:dataDict];
    
    if (![CTConfigurationManager deviceDefaultsConfigured])
    {
        [CTConfigurationManager initializeDefaults];
    }
    
    [CTConfigurationManager setDeviceToken:fcmToken];
    [CTConfigurationManager registerDeviceWithServerAPIVersion:@"v1" libraryCode:libraryCode];
}


- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}

// Receive displayed notifications for iOS 10 devices.
// Handle incoming notification messages while app is in the foreground.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
  NSDictionary *userInfo = notification.request.content.userInfo;

  // With swizzling disabled you must let Messaging know about the message, for Analytics
  // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];

  // Print message ID.
  if (userInfo[kGCMMessageIDKey]) {
    NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
  }

  // Print full message.
  NSLog(@"%@", userInfo);

  // Change this to your preferred presentation option
  completionHandler(UNNotificationPresentationOptionAlert);
}

// Handle notification messages after display notification is tapped by the user.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void(^)(void))completionHandler {
  NSDictionary *userInfo = response.notification.request.content.userInfo;
  if (userInfo[kGCMMessageIDKey]) {
    NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
  }

  // Print full message.
  NSLog(@"%@", userInfo);

  completionHandler();
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
   
}

- (void)updateLocalAccounts {

       NSDictionary *configLibraryInformation    = [CTConfigurationManager JSONConfigurationFileAsDictionary:@"config.json"];

    
       //Initialize our account manager
       accountManager = [CTAccountManager sharedInstance];
       [accountManager setAPIVersion:@"v1" libraryCode:libraryCode];
       [accountManager updateStoredAccountInformation:NO];
}

@end

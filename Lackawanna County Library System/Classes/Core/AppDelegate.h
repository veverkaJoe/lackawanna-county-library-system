//
//  AppDelegate.h
//  CapiraMobile Ready
//
//  Created by Michael on 3/14/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>
@import Firebase;

@interface AppDelegate : UIResponder <FIRMessagingDelegate, UIApplicationDelegate, UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;

@end


//
//  CTViewController.m
//  KCKPL
//
//  Created by Michael Berse on 12/2/16.
//  Copyright © 2016 Michael Berse. All rights reserved.
//

#import "CMRViewController.h"
#import <EventKit/EventKit.h>
#import <CTSearchManager/CTSearchManager.h>
#import "CMRSearchCatalogViewController.h"
#import "CTBarcodeScannerViewController.h"
#import "CMRItemDetailsViewController.h"
#import "OrderedDictionary.h"
@import Firebase;

@interface CMRViewController ()
{
    
}
@end

@implementation CMRViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //NSDictionary for settings
    configLibraryInformation    = [CTConfigurationManager JSONConfigurationFileAsDictionary:@"config.json"];
    
    //Initialize our account manager
    accountManager = [CTAccountManager sharedInstance];
    accountManager.delegate = self;
    [accountManager setAPIVersion:@"v1" libraryCode:[configLibraryInformation objectForKey:@"code"]];
    
    //Layout our graphics/colors for UI
    [self layoutUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //Ensure Account Delegate reset to current view when appearing again
    accountManager.delegate = self;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.searchBar setShowsCancelButton:NO animated:YES];
    [self handleSearch:self.searchBar];
}

- (void)handleSearch:(UISearchBar *)searchBar {
    
    NSLog(@"User searched for %@", searchBar.text);
    if(([self.searchBar.text length] < 1) || ([self.searchBar.text isEqualToString:@" "])){
        self.searchBar.text = @"";
        [self.searchBar resignFirstResponder];
        return;
    }
    [self.searchBar resignFirstResponder]; // if you want the keyboard to go away
    CMRSearchCatalogViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"Search Catalog"];
    vc.quickSearchTerm = self.searchBar.text;
    [self.navigationController pushViewController:vc animated:YES];
    
}



- (void)layoutUI
{
    
    self.searchBar.placeholder = NSLocalizedString(@"Search the Catalog - Enter Keywords", "");
    
    @try {
        
        _topNavBar.translucent          = NO;
        _bottomTabBar.translucent       = NO;
        
        //Set the bar colors
        _topNavBar.barTintColor         = [UIColor colorWithRed:[[[configLibraryInformation objectForKey:@"barTintColor"] objectForKey:@"red"]   floatValue]/255.0f
                                                          green:[[[configLibraryInformation objectForKey:@"barTintColor"] objectForKey:@"green"] floatValue]/255.0f
                                                           blue:[[[configLibraryInformation objectForKey:@"barTintColor"] objectForKey:@"blue"]  floatValue]/255.0f
                                                          alpha:1.0f];
        
        _bottomTabBar.barTintColor      = [UIColor colorWithRed:[[[configLibraryInformation objectForKey:@"barTintColor"] objectForKey:@"red"]   floatValue]/255.0f
                                                          green:[[[configLibraryInformation objectForKey:@"barTintColor"] objectForKey:@"green"] floatValue]/255.0f
                                                           blue:[[[configLibraryInformation objectForKey:@"barTintColor"] objectForKey:@"blue"]  floatValue]/255.0f
                                                          alpha:1.0f];
        
        
        //Configure Search Bar
        _searchBar.barTintColor         = [UIColor colorWithRed:[[[configLibraryInformation objectForKey:@"barTintColor"] objectForKey:@"red"]   floatValue]/255.0f
                                                          green:[[[configLibraryInformation objectForKey:@"barTintColor"] objectForKey:@"green"] floatValue]/255.0f
                                                           blue:[[[configLibraryInformation objectForKey:@"barTintColor"] objectForKey:@"blue"]  floatValue]/255.0f
                                                          alpha:1.0f];
        
        
        for(UIView *subView in [self.searchBar subviews]) {
            if([subView conformsToProtocol:@protocol(UITextInputTraits)]) {
                [(UITextField *)subView setReturnKeyType: UIReturnKeyDone];
                // always force return key to be enabled
                [(UITextField *)subView setEnablesReturnKeyAutomatically:NO];
            } else {
                for(UIView *subSubView in [subView subviews]) {
                    if([subSubView conformsToProtocol:@protocol(UITextInputTraits)]) {
                        [(UITextField *)subSubView setReturnKeyType: UIReturnKeyDone];
                        [(UITextField *)subSubView setEnablesReturnKeyAutomatically:NO];
                    }
                }
            }
        }
        
        //Special colorization of white background for iOS 13
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"13.0"))
        {
            self.searchBar.searchTextField.backgroundColor = [UIColor whiteColor];
        }
        
        //Configure top navigation bar
        _topNavBar.topItem.title = @"-";
        _topNavBar.titleTextAttributes = @{ NSForegroundColorAttributeName: [UIColor colorWithRed:[[[configLibraryInformation objectForKey:@"barFontColor"] objectForKey:@"red"]   floatValue]/255.0f
                                                                                            green:[[[configLibraryInformation objectForKey:@"barFontColor"] objectForKey:@"green"] floatValue]/255.0f
                                                                                             blue:[[[configLibraryInformation objectForKey:@"barFontColor"] objectForKey:@"blue"]  floatValue]/255.0f
                                                                                            alpha:1.0f] };
        
        
        
        //Configure Bottom Tab Bar
        NSMutableArray *tabBarItems = [NSMutableArray new];
        
        [tabBarItems addObject:[[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"Home","")
                                                             image:[[UIImage imageNamed:@"handheld_footer_home@3x.png"]
                                                                    imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                                     selectedImage:[[UIImage imageNamed:@"handheld_footer_home@3x.png"]
                                                                    imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]]];
        
        
        [tabBarItems addObject:[[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"Contact","")
                                                             image:[[UIImage imageNamed:@"handheld_footer_contact@3x.png"]
                                                                    imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                                     selectedImage:[[UIImage imageNamed:@"handheld_footer_contact@3x.png"]
                                                                    imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                ]];
        
        [tabBarItems addObject:[[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"Website","")
                                                             image:[[UIImage imageNamed:@"handheld_footer_website@3x.png"]
                                                                    imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                                     selectedImage:[[UIImage imageNamed:@"handheld_footer_website@3x.png"]
                                                                    imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                ]];
        
        _bottomTabBar.items = tabBarItems;
        
        [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }
                                                 forState:UIControlStateNormal];
        
        
        //Create "back" bar button and association navigation item for bar
        UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"button_back.png"] style:UIBarButtonItemStylePlain target:self action:@selector(navigateBack:)];
        leftButton.tintColor = [UIColor whiteColor];
        
        UINavigationItem *navItem = [[UINavigationItem alloc] init];
        
        //Add in scan bar to appropriate screens
        UIViewController *currentVC = self.navigationController.visibleViewController;
        if ([currentVC isKindOfClass:[CMRSearchCatalogViewController class]] ||[currentVC isKindOfClass:[CMRItemDetailsViewController class]] ){
            UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"handheld_icon_scanisbn@3x.png"] style:UIBarButtonItemStylePlain target:self action:@selector(scanISBN:)];
            rightButton.tintColor = [UIColor whiteColor];
            navItem.rightBarButtonItem = rightButton;
        }
        
        navItem.leftBarButtonItem = leftButton;
        navItem.hidesBackButton = YES;
        
        //Add to top navigation bar
        [_topNavBar pushNavigationItem:navItem animated:NO];
        
    } @catch (NSException *exception) {
        NSLog(@"Exception in layoutUI: %@", exception.reason);
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //Scrollview - Auto resize
    CGRect contentRect = CGRectZero;
    for (UIView *view in self.scrollView.subviews) {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    self.scrollView.contentSize = contentRect.size;
    
    //Allow Event Reminders?
    if ([[configLibraryInformation objectForKey:@"allowEventReminders"] boolValue])
    {
        EKEventStore *store = [[EKEventStore alloc] init];
        [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {}];
    }
}

- (IBAction)navigateBack:(id)sender
{
    [FIRAnalytics logEventWithName:@"user_interaction"
        parameters:@{@"name": @"Back"
    }];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)navigateHome:(id)sender
{
    [FIRAnalytics logEventWithName:@"user_interaction"
        parameters:@{@"name": @"Home"
    }];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)dismissScreen:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)showWebsite:(id)sender
{
    [FIRAnalytics logEventWithName:@"user_interaction"
        parameters:@{@"name": @"Home Website"
    }];
    [self displayBrowser:[[configLibraryInformation objectForKey:@"websites"] objectForKey:@"homepage"]];
}

- (IBAction)showDirections:(id)sender
{
    [FIRAnalytics logEventWithName:@"user_interaction"
        parameters:@{@"name": @"Show Directions"
    }];
    OrderedDictionary *addresses = [configLibraryInformation objectForKey:@"addresses"];
    NSLog(@"Addresses %@", addresses);
    NSLog(@"ALL KEYS %@", [addresses allKeys]);
    
    if ([[addresses allKeys] count] == 1){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.apple.com/?daddr=%@", [CTUtilities encodeURLString:[addresses objectForKey:@"main"]]]] options:@{} completionHandler:nil];
    }
    else if ([[addresses allKeys] count] > 1){
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(@"Choose Destination","")
                                              message:NSLocalizedString(@"Please select a destination.","")
                                              preferredStyle:UIAlertControllerStyleActionSheet];
        
        for (NSString *key in [addresses allKeys])
        {
            UIAlertAction *action = [UIAlertAction
                                     actionWithTitle:key
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction *action)
                                     {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.apple.com/?daddr=%@", [CTUtilities encodeURLString:[addresses objectForKey:key]]]] options:@{} completionHandler:nil];
            }];
            
            [alertController addAction:action];
        }
        
        //Add Cancel Button
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"Cancel","")
                                       style:UIAlertActionStyleCancel
                                       handler:nil];
        
        [alertController addAction:cancelAction];
        
        //Show to user
        UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
        alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
        alertPopoverPresentationController.sourceView = self.view;
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}


- (CGRect)centerViewRect:(UIView *)view
{
    CGRect sourceRect = CGRectZero;
    sourceRect.origin.x = CGRectGetMidX(view.bounds)-view.frame.origin.x/2.0;
    sourceRect.origin.y = CGRectGetMidY(view.bounds)-view.frame.origin.y/2.0;
    return sourceRect;
}

- (IBAction)showContactOptions:(id)sender
{
    [FIRAnalytics logEventWithName:@"user_interaction"
        parameters:@{@"name": @"Show Contact Options"
    }];
    NSDictionary *libraryPhones = [configLibraryInformation objectForKey:@"phones"];
    NSDictionary *libraryEmails = [configLibraryInformation objectForKey:@"emails"];
    
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:NSLocalizedString(@"Contact Us","")
                                          message:NSLocalizedString(@"How would you like to reach us?","")
                                          preferredStyle:UIAlertControllerStyleActionSheet];
    
    //Can we call?
    if ([libraryPhones count] >= 1)
    {
        UIAlertAction *callAction = [UIAlertAction
                                     actionWithTitle:NSLocalizedString(@"Call Us", "")
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction *action)
                                     {
            [self dialPhone:nil];
        }];
        
        [alertController addAction:callAction];
        
    }
    
    //Can we email?
    if ([libraryEmails count] >= 1)
    {
        UIAlertAction *action = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"Email Us","")
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction *action)
                                 {
            [self sendEmail:nil];
        }];
        
        [alertController addAction:action];
        
    }
    
    //Can we text?
    if ([[configLibraryInformation objectForKey:@"smsNumber"] length] >= 1)
    {
        UIAlertAction *action = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"Text Us", "")
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction *action)
                                 {
            [self sendSMS:nil];
        }];
        
        [alertController addAction:action];
        
    }
    
    //Add Cancel Button
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel","")
                                   style:UIAlertActionStyleCancel
                                   handler:nil];
    
    [alertController addAction:cancelAction];
    
    
    //Show to user
    UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
    alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
    alertPopoverPresentationController.sourceView = self.view;
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)sendSMS:(id)sender
{
    [FIRAnalytics logEventWithName:@"user_interaction"
        parameters:@{@"name": @"Send SMS"
    }];
    
    NSString *smsNumber = [NSString stringWithFormat:@"sms:%@", [configLibraryInformation objectForKey:@"smsNumber"]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:smsNumber]  options:@{} completionHandler:nil];
    
}

- (IBAction)sendEmail:(id)sender
{
    [FIRAnalytics logEventWithName:@"user_interaction"
        parameters:@{@"name": @"Send Email"
    }];
    
    //Can we send email?
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"mailto:"]])
    {
        NSDictionary *emailAddresses = [configLibraryInformation objectForKey:@"emails"];
        
        //How many email addresses do we have?
        
        //If one, open it
        if ([[emailAddresses allKeys] count] == 1)
        {
            NSString *email = [NSString stringWithFormat:@"mailto:%@", [emailAddresses objectForKey:@"main"]];
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]  options:@{} completionHandler:nil];
        }
        
        //Multiple emails? Prompt user
        else if ([[emailAddresses allKeys] count] > 1)
        {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:NSLocalizedString(@"Choose Email","")
                                                  message:NSLocalizedString(@"Please select an email address.","")
                                                  preferredStyle:UIAlertControllerStyleActionSheet];
            
            for (NSString *key in [[emailAddresses allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)])
            {
                UIAlertAction *action = [UIAlertAction
                                         actionWithTitle:key
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction *action)
                                         {
                    NSString *email = [NSString stringWithFormat:@"mailto:%@", key];
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]  options:@{} completionHandler:nil];
                }];
                
                [alertController addAction:action];
            }
            
            //Add Cancel Button
            UIAlertAction *cancelAction = [UIAlertAction
                                           actionWithTitle:NSLocalizedString(@"Cancel", "")
                                           style:UIAlertActionStyleCancel
                                           handler:nil];
            
            [alertController addAction:cancelAction];
            
            
            //Show to user
            UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
            alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
            alertPopoverPresentationController.sourceView = self.view;
            [self presentViewController:alertController animated:YES completion:nil];
        }
        
    }else{
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"No Email Client"
                                              message:@"This device does not support sending emails."
                                              preferredStyle:UIAlertControllerStyleActionSheet];
        
        
        //Add Cancel Button
        
        [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", "")
                                                            style:UIAlertActionStyleCancel
                                                          handler:nil]
         ];
        
        
        //Show to user
        UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
        alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
        alertPopoverPresentationController.sourceView = self.view;
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}


- (IBAction)dialPhone:(id)sender
{
    [FIRAnalytics logEventWithName:@"user_interaction"
        parameters:@{@"name": @"Dial Phone"
    }];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://"]])
    {
        NSDictionary *phoneAddresses = [configLibraryInformation objectForKey:@"phones"];
        
        
        if ([[phoneAddresses allKeys] count] == 1)
        {
            NSString *phone = [NSString stringWithFormat:@"tel://%@", [phoneAddresses objectForKey:@"main"]];
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phone]  options:@{} completionHandler:nil];
        }
        
        //Multiple emails? Prompt user
        else if ([[phoneAddresses allKeys] count] > 1)
        {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:NSLocalizedString(@"Choose Phone", "")
                                                  message:NSLocalizedString(@"Please select a number.", "")
                                                  preferredStyle:UIAlertControllerStyleActionSheet];
            
            for (NSString *key in [[phoneAddresses allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)])
            {
                UIAlertAction *action = [UIAlertAction
                                         actionWithTitle:key
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction *action)
                                         {
                    NSString *phone = [NSString stringWithFormat:@"tel://%@", [phoneAddresses objectForKey:key]];
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phone] options:@{} completionHandler:nil];
                }];
                
                [alertController addAction:action];
            }
            
            //Add Cancel Button
            UIAlertAction *cancelAction = [UIAlertAction
                                           actionWithTitle:NSLocalizedString(@"Cancel", "")
                                           style:UIAlertActionStyleCancel
                                           handler:nil];
            
            [alertController addAction:cancelAction];
            
            
            //Show to user
            UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
            alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
            alertPopoverPresentationController.sourceView = self.view;
            [self presentViewController:alertController animated:YES completion:nil];
        }
        
    }else{
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"No Phone Client"
                                              message:@"This device does not support making phone calls."
                                              preferredStyle:UIAlertControllerStyleActionSheet];
        
        
        //Add Cancel Button
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel"
                                                            style:UIAlertActionStyleCancel
                                                          handler:nil]
         ];
        
        
        //Show to user
        UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
        alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
        alertPopoverPresentationController.sourceView = self.view;
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}

- (void)displayBrowser:(NSString *)URLString
{
    SFSafariViewController *safariVC = [[SFSafariViewController alloc]initWithURL:[NSURL URLWithString:URLString] entersReaderIfAvailable:NO];
    //safariVC.delegate = self;
    [self presentViewController:safariVC animated:NO completion:nil];
}



#pragma mark -
#pragma mark STANDARD_DELEGATES
-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

//OVERRIDE FOR CUSTOM APP FUNCTIONS
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    //Get index of tapped button
    NSUInteger pressedIndex = [[self.bottomTabBar items] indexOfObject:item];
    switch (pressedIndex) {
        case 0:
            [self navigateHome:nil];
            break;
        case 1:
            [self showContactOptions:nil];
            break;
        case 2:
            [self showWebsite:nil];
            break;
    }
}

- (void)didScanBarcode:(NSString *)barcode
{
    CTCatalogServices *webService = [[CTCatalogServices alloc] initWithVersion:@"v1" libraryCode:[self->configLibraryInformation objectForKey:@"code"]];
    
    webService.delegate = self;
    
    [webService executeISBNLookup:barcode];
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:NSLocalizedString(@"Searching...", "")];
}

-(void)checkCameraPermissions{
        AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        if(authStatus == AVAuthorizationStatusAuthorized)
        {
            NSLog(@"%@", @"You have camera access");
        }
        else if(authStatus == AVAuthorizationStatusDenied)
        {
            NSLog(@"%@", @"Denied camera access");
        
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:NSLocalizedString(@"Permissions", "")
                                                  message:NSLocalizedString(@"Application doesn't have permission to use Camera, please change privacy settings. Without permissions this feature will not function.", "")
                                                  preferredStyle:UIAlertControllerStyleActionSheet];
            
            
            UIAlertAction *permissionsAction = [UIAlertAction
                                                actionWithTitle:NSLocalizedString(@"Allow Permissions", "")
                                                style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction *action)
                                                {
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                
            }];
            [alertController addAction:permissionsAction];
            
            //Add Cancel Button
            UIAlertAction *cancelAction = [UIAlertAction
                                           actionWithTitle:NSLocalizedString(@"Close", "")
                                           style:UIAlertActionStyleCancel
                                           handler:nil];

            
            [alertController addAction:cancelAction];
            
            
            //Show to user
            UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
            alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
            alertPopoverPresentationController.sourceView = self.view;
            
            [self presentViewController:alertController animated:YES completion:nil];
            
            return;
        
        }
        else if(authStatus == AVAuthorizationStatusRestricted)
        {
            NSLog(@"%@", @"Restricted, normally won't happen");
        }
        else
        {
            NSLog(@"%@", @"Camera access unknown error.");
        }
}


- (IBAction)scanISBN:(id)sender
{
    
    [FIRAnalytics logEventWithName:@"user_interaction"
        parameters:@{@"name": @"Scan ISBN"
    }];
    [self checkCameraPermissions];
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:NSLocalizedString(@"Scan ISBN", "")
                                          message:NSLocalizedString(@"How would you like to input the ISBN?", "")
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel","")
                                   style:UIAlertActionStyleCancel
                                   handler:nil];
    
    UIAlertAction *cameraAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Camera", "")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * _Nonnull action) {
        CTBarcodeScannerViewController *vc = [[CTBarcodeScannerViewController alloc] init];
        vc.delegate = self;
        [self presentViewController:vc animated:YES completion:nil];
    }];
    
    UIAlertAction *manualAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Enter Manually", "")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * _Nonnull action) {
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle: NSLocalizedString(@"Enter ISBN", "")
                                                                                  message: nil
                                                                           preferredStyle:UIAlertControllerStyleAlert];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = NSLocalizedString(@"ISBN (No Spaces)", "");
            textField.textColor = [UIColor blueColor];
            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            textField.borderStyle = UITextBorderStyleRoundedRect;
        }];
        [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Search", "") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            NSArray * textfields = alertController.textFields;
            UITextField * isbnfield = textfields[0];
            
            CTCatalogServices *webService = [[CTCatalogServices alloc] initWithVersion:@"v1" libraryCode:[self->configLibraryInformation objectForKey:@"code"]];
            
            
            webService.delegate = self;
            
            [webService executeISBNLookup:isbnfield.text];
            
            [DejalBezelActivityView activityViewForView:self.view withLabel:NSLocalizedString(@"Searching...", "")];
            
        }]];
        
        UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
        alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
        alertPopoverPresentationController.sourceView = self.view;
        
        [self presentViewController:alertController animated:YES completion:nil];
    }];
    
    
    [alertController addAction:cancelAction];
    [alertController addAction:cameraAction];
    [alertController addAction:manualAction];
    
    UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
    alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
    alertPopoverPresentationController.sourceView = self.view;
    
    [self presentViewController:alertController animated:YES completion:nil];
    
    
}

- (void)didReceiveLookup:(BOOL)success lookupResult:(CTSearchResult *)result
{
    [DejalBezelActivityView removeViewAnimated:YES];
    
    if (success)
    {
        CMRItemDetailsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"Item Details"];
        vc.displayItem = result;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(@"Item Not Found", "")
                                              message:NSLocalizedString(@"We were not able to find this item in our catalog.", "")
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"Close", "")
                                       style:UIAlertActionStyleCancel
                                       handler:nil];
        
        
        [alertController addAction:cancelAction];
        
        UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
        alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
        alertPopoverPresentationController.sourceView = self.view;
        
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
}

- (void)didFailWithError:(NSString *)errorMsg httpStatusCode:(NSInteger)statusCode
{
    
}
@end

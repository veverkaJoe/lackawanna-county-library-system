//
//  CMRDigitalServicesViewController.m
//  CapiraMobile Ready
//
//  Created by Michael on 5/8/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import "CMRDigitalServicesViewController.h"
#import "UIImageView+WebCache.h"
#import <SDWebImage/SDWebImage.h>
#import "CTDigitalService.h"
@import Firebase;

@interface CMRDigitalServicesViewController ()
{
    NSMutableArray *listOfServices;
}
@end

@implementation CMRDigitalServicesViewController

 -(void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.topNavBar.topItem.title = NSLocalizedString(@"Digital Resources", "");
  
    listOfServices = [NSMutableArray new];
     
     NSArray *serviceDictionaries = [configLibraryInformation objectForKey:@"digitalServices"];
    

     
     for (NSDictionary *serviceDict in serviceDictionaries)
     {
         CTDigitalService *service = [CTDigitalService new];
                
         service.serviceName = [serviceDict objectForKey:@"name"];
         service.serviceImage = [serviceDict objectForKey:@"image"];
         service.serviceDescription =[serviceDict objectForKey:@"description"];
         service.serviceURL = [serviceDict objectForKey:@"url"];
                
         [listOfServices addObject:service];
     }
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [FIRAnalytics logEventWithName:kFIREventScreenView parameters:@{
        kFIRParameterScreenName: @"Digital Resources",
        kFIRParameterScreenClass: @"Digital Resources"
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CTDigitalService *service = [listOfServices objectAtIndex:indexPath.row];

    [self displayBrowser:service.serviceURL];
   // [self performSelector:NSSelectorFromString(service.serviceSelector)];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [listOfServices count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return 200;
    }else{
        return 200;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"cellService";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    UILabel *name = (UILabel *)[cell viewWithTag:1];
    
    UILabel *description = (UILabel *)[cell viewWithTag:2];
    
    UIImageView *imgLogo = (UIImageView *)[cell viewWithTag:4];
    
    CTDigitalService *temp = [listOfServices objectAtIndex:indexPath.row];
    
    //    name.text = [NSString stringWithFormat:@"%@ at Booth #%@", temp.exhibitorName, temp.exhibitorBooth];
    
    name.text = [NSString stringWithFormat:@"%@", temp.serviceName];
    description.text = temp.serviceDescription;
    
    
    
    // Fix for iOS 7 to clear backgroundColor
    cell.backgroundColor = [UIColor clearColor];
    cell.backgroundView = [UIView new];
    cell.selectedBackgroundView = [UIView new];

    
    [imgLogo sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://webservices.capiramobile.com/configurations/capiraready/%@/icons/%@", [configLibraryInformation objectForKey:@"code"], temp.serviceImage]]
                         placeholderImage:[UIImage imageNamed:nil]
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                    if (imgLogo.image.size.width < 20.0)
                                    {
                                        [imgLogo setImage:[UIImage imageNamed:nil]];
                                    }
                                }];
    
    return cell;
    
}




@end

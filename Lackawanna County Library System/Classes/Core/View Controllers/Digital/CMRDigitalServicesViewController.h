//
//  CMRDigitalServicesViewController.h
//  CapiraMobile Ready
//
//  Created by Michael on 5/8/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import "CMRViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CMRDigitalServicesViewController : CMRViewController
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@end

NS_ASSUME_NONNULL_END

//
//  CMREventDetailsViewController.m
//  CapiraMobile Ready
//
//  Created by Michael on 5/7/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import "CMREventDetailsViewController.h"
@import Firebase;

@interface CMREventDetailsViewController ()

@end

@implementation CMREventDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.topNavBar.topItem.title = NSLocalizedString(@"Event Details", "");
 

    if (![self.selectedEvent canAddEventToCalendar])
    {
        self.btnReminder.hidden = YES;
    }
    
    if (!self.selectedEvent.allowsSignup)
    {
        self.btnRegister.hidden = YES;
    }
 
    
    //Set primary Label Data
    self.lblTitle.text = self.selectedEvent.title;
    
    self.txtData.text = [NSString stringWithFormat:@"%@\n", self.selectedEvent.date];
    self.txtData.text = [self.txtData.text stringByAppendingString:[NSString stringWithFormat:@"%@\n", self.selectedEvent.time]];
    self.txtData.text = [self.txtData.text stringByAppendingString:[NSString stringWithFormat:@"%@\n", self.selectedEvent.location]];
    self.txtData.text = [self.txtData.text stringByAppendingString:[NSString stringWithFormat:@"%@\n", self.selectedEvent.library]];
    
    //Append other data
    if ([self.selectedEvent.agegroup length] > 0)
    {
        self.txtData.text = [self.txtData.text stringByAppendingString:[NSString stringWithFormat:@"Ages: %@\n", self.selectedEvent.agegroup]];
    }
    
    if ([self.selectedEvent.type length] > 0)
    {
        self.txtData.text = [self.txtData.text stringByAppendingString:[NSString stringWithFormat:@"Type/Category: %@\n", self.selectedEvent.type]];
    }
    
    if ([self.selectedEvent.presenter length] > 0)
    {
        self.txtData.text = [self.txtData.text stringByAppendingString:[NSString stringWithFormat:@"Presenter: %@\n", self.selectedEvent.presenter]];
    }
    

 //   self.lblData.text = [self.lblData.text stringByAppendingString:[NSString stringWithFormat:@"\n\nSummary\n\n%@", self.selectedEvent.summary]];
    self.txtData.text = [self.txtData.text stringByAppendingString:[NSString stringWithFormat:@"\n\nSummary\n\n%@", self.selectedEvent.summary]];

    
    //Resize data label
   // [self.lblData sizeToFit];
    [self.txtData sizeToFit];
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [FIRAnalytics logEventWithName:kFIREventScreenView parameters:@{
        kFIRParameterScreenName: @"Event Details",
        kFIRParameterScreenClass: @"Event Details"
    }];
}

- (IBAction)onRegister:(id)sender
{
    [FIRAnalytics logEventWithName:@"user_interaction"
        parameters:@{@"name": @"Register Event"
    }];
    
    [self displayBrowser:self.selectedEvent.link];
}

- (IBAction)onReminder:(id)sender
{
    [FIRAnalytics logEventWithName:@"user_interaction"
        parameters:@{@"name": @"Add Event to Calendar"
    }];
    
    BOOL result =  [self.selectedEvent addEventToCalendar];
    NSString *msg = (result) ? @"Reminder added to calendar successfully." : @"Unable to add reminder to device calendar. Did you enable Calendar Permissions?";
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Add Reminder"
                                          message:msg
                                          preferredStyle:UIAlertControllerStyleActionSheet];
    
    //Add Cancel Button
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:@"Close"
                                   style:UIAlertActionStyleCancel
                                   handler:nil];
    
    [alertController addAction:cancelAction];
    
    
    //Show to user
    UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
    alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
    alertPopoverPresentationController.sourceView = self.view;
    [self presentViewController:alertController animated:YES completion:nil];
    
    return;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

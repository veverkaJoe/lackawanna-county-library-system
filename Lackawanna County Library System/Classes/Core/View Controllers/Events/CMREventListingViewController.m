//
//  CMREventListingViewController.m
//  CapiraMobile Ready
//
//  Created by Michael on 4/3/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import "CMREventListingViewController.h"
#import "CMREventDetailsViewController.h"
@import Firebase;

@interface CMREventListingViewController ()
{
    CTEventManager *eventManager;
    NSMutableArray *eventArray;
}
@end

@implementation CMREventListingViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.topNavBar.topItem.title = NSLocalizedString(@"Events", "");
 

    //Config Events Manager
    eventArray      = [NSMutableArray new];
    eventManager    = [[CTEventManager alloc] initWithVersion:@"v1" libraryCode:[configLibraryInformation objectForKey:@"code"]];
    
    eventManager.delegate = self;
    
    //Config tableview
    self.tableView.alpha = 0.0f;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"13.0"))
    {
        //Bug fix for iOS 13 and Search Bars
        self.searchBar.searchTextField.backgroundColor = [UIColor whiteColor];
    }
    
    //Config Search Bar
    self.searchBar.placeholder = NSLocalizedString(@"Search", "");

    
    for(UIView *subView in [self.searchBar subviews]) {
        if([subView conformsToProtocol:@protocol(UITextInputTraits)]) {
            [(UITextField *)subView setReturnKeyType: UIReturnKeyDone];
            // always force return key to be enabled
            [(UITextField *)subView setEnablesReturnKeyAutomatically:NO];
        } else {
            for(UIView *subSubView in [subView subviews]) {
                if([subSubView conformsToProtocol:@protocol(UITextInputTraits)]) {
                    [(UITextField *)subSubView setReturnKeyType: UIReturnKeyDone];
                    [(UITextField *)subSubView setEnablesReturnKeyAutomatically:NO];
                }
            }
        }
    }
    
    //Load initial list of events
    [eventManager executeWithQuery:@""];
    [DejalBezelActivityView activityViewForView:self.view withLabel:NSLocalizedString(@"Loading...", "")];

    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [FIRAnalytics logEventWithName:kFIREventScreenView parameters:@{
        kFIRParameterScreenName: @"Events",
        kFIRParameterScreenClass: @"Events"
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark UISEARCHBAR_DELEGATES


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.searchBar setShowsCancelButton:NO animated:YES];
    [self handleSearch:self.searchBar];
}
- (void)handleSearch:(UISearchBar *)searchBar {
    if ([searchBar.text length] == 0)
    {
       self.searchBar.text = @"";
       [self.searchBar resignFirstResponder];
        return;
    }
    [self.searchBar resignFirstResponder];
    //Hide Tableview
    [UIView animateWithDuration:0.4
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.tableView.alpha = 0.0;
                     }
                     completion:^(BOOL finished){
                        [self->eventManager executeWithQuery:searchBar.text];
                         [DejalBezelActivityView activityViewForView:self.view withLabel:NSLocalizedString(@"Searching...", "")];
                     }
     ];
 
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    self.searchBar.text = @"";
}


#pragma mark -
#pragma mark UITABLEVIEW_DELEGATES
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [eventArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 150;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CTLibraryEvent *event = [eventArray objectAtIndex:indexPath.row];

    CMREventDetailsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"Event Details"];
    vc.selectedEvent = event;
    
    [self.navigationController pushViewController:vc animated:YES];
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    static NSString *MyIdentifier = @"EventCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:MyIdentifier];
    }
    
    UILabel *lblTitle       = (UILabel *)[cell viewWithTag:100];
    UILabel *lblDate        = (UILabel *)[cell viewWithTag:101];
    UILabel *lblTime        = (UILabel *)[cell viewWithTag:102];
    UILabel *lblLocation    = (UILabel *)[cell viewWithTag:103];
    UILabel *lblLibrary     = (UILabel *)[cell viewWithTag:104];
    
    CTLibraryEvent *rowEvent = [eventArray objectAtIndex:indexPath.row];
    
    lblTitle.text       = rowEvent.title;
    lblDate.text        = rowEvent.date;
    lblTime.text        = rowEvent.time;
    lblLocation.text    = rowEvent.location;
    lblLibrary.text     = rowEvent.library;
    
    return cell;
}


#pragma mark -
#pragma mark CTEVENTSMANAGER_DELEGATES
- (void)didReceiveEvents:(NSArray *)events
{
    [DejalBezelActivityView removeViewAnimated:YES];
    eventArray = [events mutableCopy];
    [self.tableView reloadData];
    
    //Animate Listing
    [UIView animateWithDuration:0.4
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.tableView.alpha = 1.0;
                     }
                     completion:^(BOOL finished){
                         
                     }
     ];
}

- (void)didFailWithError:(NSString *)errorMsg httpStatusCode:(NSInteger)statusCode
{
    [DejalBezelActivityView removeViewAnimated:YES];
    
    //Get last good event list
    eventArray = [[eventManager getCachedEvents] mutableCopy];
    
    [self.tableView reloadData];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Error"
                                          message:errorMsg
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:@"Close"
                                   style:UIAlertActionStyleCancel
                                   handler:nil];
    

    [alertController addAction:cancelAction];

    UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
    alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
    alertPopoverPresentationController.sourceView = self.view;
    
    [self presentViewController:alertController animated:YES completion:nil];
}

@end

//
//  CMREventListingViewController.h
//  CapiraMobile Ready
//
//  Created by Michael on 4/3/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import "CMRViewController.h"
#import <CTEventsManager/CTLibraryEvent.h>
#import <CTEventsManager/CTEventManager.h>
NS_ASSUME_NONNULL_BEGIN

@interface CMREventListingViewController : CMRViewController <CTEventsManagerDelegate, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;


@end

NS_ASSUME_NONNULL_END

//
//  CMREventDetailsViewController.h
//  CapiraMobile Ready
//
//  Created by Michael on 5/7/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import "CMRViewController.h"
#import <CTEventsManager/CTLibraryEvent.h>

NS_ASSUME_NONNULL_BEGIN

@interface CMREventDetailsViewController : CMRViewController
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblData;
@property (nonatomic, weak) IBOutlet UITextView *txtData;

@property (nonatomic, weak) IBOutlet UIButton *btnRegister;
@property (nonatomic, weak) IBOutlet UIButton *btnReminder;

@property (nonatomic, strong) CTLibraryEvent *selectedEvent;

- (IBAction)onRegister:(id)sender;
- (IBAction)onReminder:(id)sender;

@end

NS_ASSUME_NONNULL_END

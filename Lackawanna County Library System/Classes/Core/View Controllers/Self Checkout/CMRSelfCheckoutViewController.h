//
//  CMRSelfCheckoutViewController.h
//  CapiraMobile Ready
//
//  Created by Michael on 6/23/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import "CMRViewController.h"
#import <ZXingObjC/ZXingObjC.h>
@interface CMRSelfCheckoutViewController : CMRViewController <MFMailComposeViewControllerDelegate, ZXCaptureDelegate, CTAccountsManagerDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic) CTPatronAccount *chosenPatron;
@property (nonatomic, strong) ZXCapture *capture;
@property (nonatomic, weak) IBOutlet UIView *scanRectView;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UILabel *lblPatronName;
@property (nonatomic, weak) IBOutlet UIButton *btnManualEntry;

- (IBAction)manualEntry:(id)sender;
- (IBAction)printReceipt:(id)sender;

@end

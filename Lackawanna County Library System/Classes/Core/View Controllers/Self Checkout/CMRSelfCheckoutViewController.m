//
//  CMRSelfCheckoutViewController.m
//  CapiraMobile Ready
//
//  Created by Michael on 6/23/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import "CMRSelfCheckoutViewController.h"
@import Firebase;

@interface CMRSelfCheckoutViewController ()
{
    // Array containing data on all of the items being checked out
    NSMutableArray  * checkoutItems;
    CGAffineTransform _captureSizeTransform;
    BOOL processingResult;
    NSMutableArray *checkoutBarcodes;
}
@end

@implementation CMRSelfCheckoutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.lblPatronName.text = self.chosenPatron.name;
    
    self.topNavBar.topItem.title = NSLocalizedString(@"Mobile Checkout", "");
    
    self.capture = [[ZXCapture alloc] init];
    self.capture.camera = self.capture.back;
    self.capture.focusMode = AVCaptureFocusModeContinuousAutoFocus;

    [self.capture.hints addPossibleFormat:kBarcodeFormatCodabar];
    [self.capture.hints addPossibleFormat:kBarcodeFormatCode128];
    [self.capture.hints addPossibleFormat:kBarcodeFormatCode39];
    [self.capture.hints addPossibleFormat:kBarcodeFormatCode93];
    [self.view.layer addSublayer:self.capture.layer];
     //[self.scanRectView.layer addSublayer:self.capture.layer];
  //  [self.scanRectView.layer addSublayer:self.capture.layer];
  //  [self.scanRectView setClipsToBounds:YES];
    [self.view bringSubviewToFront:self.scanRectView];
    
    processingResult = NO;

    /*
    self.capture = [[ZXCapture alloc] init];
    self.capture.camera = self.capture.back;
    self.capture.focusMode = AVCaptureFocusModeContinuousAutoFocus;
    [self.scanRectView.layer addSublayer:self.capture.layer];
    */
    checkoutItems = [NSMutableArray new];
    checkoutBarcodes = [NSMutableArray new];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.capture.delegate = self;
    [self applyOrientation];
    
    self.btnManualEntry.titleLabel.minimumScaleFactor = 0.2f;
    self.btnManualEntry.titleLabel.numberOfLines = 1;
    self.btnManualEntry.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.btnManualEntry.titleLabel.lineBreakMode = NSLineBreakByClipping;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [FIRAnalytics logEventWithName:kFIREventScreenView parameters:@{
        kFIRParameterScreenName: @"Mobile Checkout",
        kFIRParameterScreenClass: @"Mobile Checkout"
    }];
    self.capture.delegate = self;
    [self applyOrientation];
}

- (BOOL)shouldAutorotate {
 //   UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
   // [self applyOrientation];
    return YES;
}

-(CGFloat) degreesToRadians:(CGFloat)degrees{

  return degrees * M_PI / 180;

}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context)
     {
         [self applyOrientation];
     }];
}

- (IBAction)printReceipt:(id)sender
{
    //GA Hit
   [FIRAnalytics logEventWithName:@"user_interaction"
        parameters:@{@"name": @"Print Self Checkout Receipt"
    }];
        MFMailComposeViewController * mailComposer      = nil;
        // Present the user with a mail composer view containing the receipt text
                mailComposer = [[MFMailComposeViewController alloc] init];
    
    if ([MFMailComposeViewController canSendMail]) {
        [mailComposer setMailComposeDelegate:self];
        [mailComposer setSubject:NSLocalizedString(@"Your Checkout Receipt", "")];
        [mailComposer setMessageBody:[self buildCheckoutReceiptText] isHTML:TRUE];
        [self presentViewController:mailComposer animated:TRUE completion:nil];
    }

}
- (NSString*) buildCheckoutReceiptText
{
    // Builds the receipt text
    NSMutableString * builder   = nil;
    // The resulting receipt text
    NSString        * result    = nil;
    
    // Initialize the string builder
    builder = [[NSMutableString alloc] init];

    // Build the receipt text
    [builder appendString:@"<h><b><u>Your Checkout Receipt</u></b></h>"];
    [builder appendString:@"<body>"];
    for (NSDictionary * currCheckout in checkoutItems)
    {
        if ([[currCheckout objectForKey:@"success"] boolValue])
        {
            [builder appendFormat:@"<p><b>%@</b><br><i>Due Date: %@</i></p>", [currCheckout objectForKey:@"title"], [currCheckout objectForKey:@"dueDate"]];
        }
    }
    [builder appendString:@"</body>"];
    
    // Copy the constructed receipt text into the result string
    result = [[NSString alloc] initWithString:builder];
    
    // Return the resulting receipt text
    return result;
}
#pragma mark - Private
- (void)applyOrientation {
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    float scanRectRotation;
    float captureRotation;
    switch (orientation) {
        case UIInterfaceOrientationPortrait:
            captureRotation = 0;
            scanRectRotation = 90.0f;
            break;
        case UIInterfaceOrientationLandscapeLeft:
            captureRotation = 90.0f;
            scanRectRotation = 180;
            break;
        case UIInterfaceOrientationLandscapeRight:
            captureRotation = 270.0f;
            scanRectRotation = 0;
            break;

        case UIInterfaceOrientationPortraitUpsideDown:
            captureRotation = 180.0f;
            scanRectRotation = 270;
            break;

        default:
            captureRotation = 180;
            scanRectRotation = 90;
            break;
    }

    self.capture.layer.frame = self.scanRectView.frame;
    [self applyRectOfInterest:orientation];
    [self.capture setRotation:scanRectRotation];

    CATransform3D transform =  CATransform3DMakeRotation([self degreesToRadians:captureRotation], 0, 0, 1.0);
    self.capture.layer.transform =transform;
    self.capture.layer.frame = self.scanRectView.frame;
}


- (void)applyRectOfInterest:(UIInterfaceOrientation)orientation {
    CGFloat scaleVideo, scaleVideoX, scaleVideoY;
    CGFloat videoSizeX, videoSizeY;
    CGRect transformedVideoRect = self.scanRectView.frame;
    if([self.capture.sessionPreset isEqualToString:AVCaptureSessionPreset1920x1080]) {
        videoSizeX = 1080;
        videoSizeY = 1920;
      //  videoSizeX = 220;
      //  videoSizeY = 211;
      //  videoSizeX = self.scanRectView.frame.size.width;
       // videoSizeY = self.scanRectView.frame.size.height;
    } else {
        videoSizeX = 720;
        videoSizeY = 1280;
       // videoSizeX = 50;
       // videoSizeY = 50;
    }
    if(UIInterfaceOrientationIsPortrait(orientation)) {
        scaleVideoX = self.scanRectView.frame.size.width / videoSizeX;
        scaleVideoY = self.scanRectView.frame.size.height / videoSizeY;
        scaleVideo = MAX(scaleVideoX, scaleVideoY);
        if(scaleVideoX > scaleVideoY) {
            transformedVideoRect.origin.y += (scaleVideo * videoSizeY - self.scanRectView.frame.size.height) / 3;
        } else {
            transformedVideoRect.origin.x += (scaleVideo * videoSizeX - self.scanRectView.frame.size.width) / 3;
        }
    } else {
        NSLog(@"LAndscape");
        scaleVideoX = self.view.frame.size.width / videoSizeY;
        scaleVideoY = self.view.frame.size.height / videoSizeX;
        scaleVideo = MAX(scaleVideoX, scaleVideoY);
        if(scaleVideoX > scaleVideoY) {
            transformedVideoRect.origin.y += (scaleVideo * videoSizeX - self.view.frame.size.height) / 2;
        } else {
            transformedVideoRect.origin.x += (scaleVideo * videoSizeY - self.view.frame.size.width) / 2;
        }
         
    }
    _captureSizeTransform = CGAffineTransformMakeScale(1/scaleVideo, 1/scaleVideo);
 //   self.capture.scanRect = CGRectApplyAffineTransform(transformedVideoRect, _captureSizeTransform);
}


- (NSString *)barcodeFormatToString:(ZXBarcodeFormat)format {
    switch (format) {
        case kBarcodeFormatAztec:
            return @"Aztec";
            
        case kBarcodeFormatCodabar:
            return @"CODABAR";
            
        case kBarcodeFormatCode39:
            return @"Code 39";
            
        case kBarcodeFormatCode93:
            return @"Code 93";
            
        case kBarcodeFormatCode128:
            return @"Code 128";
            
        case kBarcodeFormatDataMatrix:
            return @"Data Matrix";
            
        case kBarcodeFormatEan8:
            return @"EAN-8";
            
        case kBarcodeFormatEan13:
            return @"EAN-13";
            
        case kBarcodeFormatITF:
            return @"ITF";
            
        case kBarcodeFormatPDF417:
            return @"PDF417";
            
        case kBarcodeFormatQRCode:
            return @"QR Code";
            
        case kBarcodeFormatRSS14:
            return @"RSS 14";
            
        case kBarcodeFormatRSSExpanded:
            return @"RSS Expanded";
            
        case kBarcodeFormatUPCA:
            return @"UPCA";
            
        case kBarcodeFormatUPCE:
            return @"UPCE";
            
        case kBarcodeFormatUPCEANExtension:
            return @"UPC/EAN extension";
            
        default:
            return @"Unknown";
    }
}

#pragma mark - ZXCaptureDelegate Methods

- (void)captureResult:(ZXCapture *)capture result:(ZXResult *)result {

    if (!result)
    {
        NSLog(@"Delegate Called");
        return;
    }
    
    
    
    if (processingResult)
    {
        return;
    }else{
        [self.capture stop];
    }
    
    [self.capture stop];
    
    CGAffineTransform inverse = CGAffineTransformInvert(_captureSizeTransform);
    NSMutableArray *points = [[NSMutableArray alloc] init];
    NSString *location = @"";
    for (ZXResultPoint *resultPoint in result.resultPoints) {
        CGPoint cgPoint = CGPointMake(resultPoint.x, resultPoint.y);
        CGPoint transformedPoint = CGPointApplyAffineTransform(cgPoint, inverse);
        transformedPoint = [self.scanRectView convertPoint:transformedPoint toView:self.scanRectView.window];
        NSValue* windowPointValue = [NSValue valueWithCGPoint:transformedPoint];
        location = [NSString stringWithFormat:@"%@ (%f, %f)", location, transformedPoint.x, transformedPoint.y];
        [points addObject:windowPointValue];
    }
    
    // We got a result. Display information about the result onscreen.
    NSString *formatString = [self barcodeFormatToString:result.barcodeFormat];
    NSString *display = [NSString stringWithFormat:@"Scanned!\n\nFormat: %@\n\nContents:\n%@\nLocation: %@", formatString, result.text, location];
    
    NSLog(@"%@", display);
    //[self.decodedLabel performSelectorOnMainThread:@selector(setText:) withObject:display waitUntilDone:YES];
    
    // Vibrate
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    
    [self.capture stop];
    
    /*
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self.capture start];
    });
     */
    
     if (![checkoutBarcodes containsObject:result.text])
     {
        [checkoutBarcodes addObject:result.text];
        [DejalBezelActivityView activityViewForView:self.view];
        [accountManager checkoutItem:result.text forAccount:self.chosenPatron];
     }
     else{
         [self.capture start];

     }
    
    
}

- (IBAction)manualEntry:(id)sender
{
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:NSLocalizedString(@"Manual Entry", "")
                                          message:nil
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = NSLocalizedString(@"Barcode", "");
        textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType = UIKeyboardTypeDefault;
     //   textField.text = @"34444925482648";
    }];
    
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Checkout", "") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * bField = textfields[0];
        [DejalBezelActivityView activityViewForView:self.view];
        [accountManager checkoutItem:bField.text forAccount:self.chosenPatron];
        
    }]];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", "")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * _Nonnull action) {
                                       
                                   }];
    
    
    [alertController addAction:cancelAction];
    
    UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
    alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
    alertPopoverPresentationController.sourceView = self.view;
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)circulationRequestSuccessful:(BOOL)success responseMessage:(NSString *)message circulationType:(CirculationAction)action originalItemId:(NSString *)itemId itemTitle:(NSString *)title dueDate:(NSString *)dueDateVal
{
    [DejalBezelActivityView removeViewAnimated:YES];
    [self.capture start];
    
    processingResult = NO;
    
    if (success)
    {
        NSLog(@"SUCCESS");
    }
    NSMutableDictionary *dataDict = [NSMutableDictionary new];
    [dataDict setObject:[NSNumber numberWithBool:success] forKey:@"success"];
    [dataDict setObject:title forKey:@"title"];
    [dataDict setObject:dueDateVal forKey:@"dueDate"];
    [dataDict setObject:itemId forKey:@"itemId"];
    
    [checkoutItems addObject:dataDict];
    [self.tableView reloadData];
}


#pragma mark -
#pragma mark UITABLEVIEW_DELEGATES
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [checkoutItems count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *MyIdentifier = @"checkoutCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    
  
    UILabel *lblItemTitle           = (UILabel *)[cell viewWithTag:100];
    UILabel *lblItemStatus           = (UILabel *)[cell viewWithTag:101];
    
    NSDictionary *dataDict = [checkoutItems objectAtIndex:indexPath.row];
    
    BOOL success = [[dataDict objectForKey:@"success"] boolValue];
    
    NSLog(@"%@", dataDict);
    if (success)
    {
        cell.backgroundColor = [UIColor whiteColor];
        lblItemStatus.text = [NSString stringWithFormat:@"Due: %@", [dataDict objectForKey:@"dueDate"]];
        lblItemTitle.text = [dataDict objectForKey:@"title"];
    }else{
        cell.backgroundColor = [UIColor colorWithRed:255.0f/255.0f green:182.0f/255.0f blue:193.0f/255.0f alpha:1];
        lblItemTitle.text = [dataDict objectForKey:@"itemId"];
        lblItemStatus.text = NSLocalizedString(@"Unable to checkout item. Please contact the library.", "");
    }
    
    return cell;
}


// --------------------------------------------------------------------------------
# pragma mark - MFMailComposer Delegate
// --------------------------------------------------------------------------------
// States that the mail composer can be dismissed
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    // Animate the mail composer dismissing
    [self dismissViewControllerAnimated:TRUE completion:nil];
    
    // If the email composer was not canceled
    if (result != MFMailComposeResultCancelled)
    {
       
        // Animate this view dismissing
        [self.navigationController popViewControllerAnimated:YES];
    }
   
}


@end


//
//  CMRHomeViewController.m
//  CapiraMobile Ready
//
//  Created by Michael on 3/18/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import "CMRHomeViewController.h"
#import "CMRDigitalCardViewController.h"
#import "CMREventListingViewController.h"
@import Firebase;

@interface CMRHomeViewController ()

@end

@implementation CMRHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Set Screen Display Title in Nav Bar
    self.topNavBar.topItem.title = NSLocalizedString(@"Welcome", "");
    self.topNavBar.topItem.leftBarButtonItem = nil;

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"HasLaunchedOnce"] && [accountManager numberOfAccounts] == 0)
    {

        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"First Setup"];
     //   vc.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentViewController:vc animated:YES completion:nil];
    }
}


- (IBAction)showEventCalendar:(id)sender
{
    [FIRAnalytics logEventWithName:@"user_interaction"
        parameters:@{@"name": @"Show Event Calendar"
    }];
    
    NSString *calendarUrl = [[configLibraryInformation objectForKey:@"websites"] objectForKey:@"calendar"];
    
    //If the library has provided a calendar page, we should load that in the browser. Otherwise, display in-app.
    if ([calendarUrl length] == 0)
    {
        CMREventListingViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"Event Listing"];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        [self displayBrowser:calendarUrl];
    }
}

- (IBAction)showDigitalLibraryCard:(id)sender
{
    [FIRAnalytics logEventWithName:@"user_interaction"
        parameters:@{@"name": @"Show Digital Library Card"
    }];
    
    //Do we have any accounts?
    if ([accountManager numberOfAccounts] == 0)
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(@"No Accounts Configured", "")
                                              message:NSLocalizedString(@"Please add your library card in My Account > Manage Accounts.", "")
                                              preferredStyle:UIAlertControllerStyleActionSheet];
        
        //Add Cancel Button
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"Close", "")
                                       style:UIAlertActionStyleCancel
                                       handler:nil];
        
        [alertController addAction:cancelAction];
        
        
        //Show to user
         //Show to user
       UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
       alertPopoverPresentationController.sourceRect = ((UIButton *) sender).bounds;
       alertPopoverPresentationController.sourceView = ((UIButton *) sender);
        
        [self presentViewController:alertController animated:YES completion:nil];
        
        return;

    }
    
    CMRDigitalCardViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"Digital Library Card"];
    //Do we have only 1 account?
    if ([accountManager numberOfAccounts] == 1)
    {
        vc.chosenPatronAccount = [[accountManager getAllStoredAccounts] objectAtIndex: 0];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        //Prompt to choose account
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(@"Choose Account", "")
                                              message:nil
                                              preferredStyle:UIAlertControllerStyleActionSheet];
        
        for (CTPatronAccount *account in [accountManager getAllStoredAccounts])
        {
            UIAlertAction *action = [UIAlertAction
                                     actionWithTitle:account.name
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction *action)
                                     {
                vc.chosenPatronAccount = [self->accountManager getAccountWithBarcode:account.barcode];
                                         [self.navigationController pushViewController:vc animated:YES];
                                     }];
            
            [alertController addAction:action];
        }
        
        //Add Cancel Button
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"Cancel", "")
                                       style:UIAlertActionStyleCancel
                                       handler:nil];
        
        [alertController addAction:cancelAction];
        
        //Show to user
        UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
        alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
        alertPopoverPresentationController.sourceView = self.view;
        [self presentViewController:alertController animated:YES completion:nil];
        
    }

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

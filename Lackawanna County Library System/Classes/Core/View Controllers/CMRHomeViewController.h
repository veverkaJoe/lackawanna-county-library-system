//
//  CMRHomeViewController.h
//  CapiraMobile Ready
//
//  Created by Michael on 3/18/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import "CMRViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CMRHomeViewController : CMRViewController

@end

NS_ASSUME_NONNULL_END

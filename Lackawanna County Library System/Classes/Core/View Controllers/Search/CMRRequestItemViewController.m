//
//  CMRRequestItemViewController.m
//  CapiraMobile Ready
//
//  Created by Michael on 5/8/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import "CMRRequestItemViewController.h"
@import Firebase;

@interface CMRRequestItemViewController ()
{
    NSArray *pickupLocationLabels;
    NSArray *pickupLocationCodes;
    NSMutableArray *volumeLabels;
    NSMutableArray *volumeCodes;
}
@end

@implementation CMRRequestItemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  
    
    volumeLabels = [NSMutableArray new];
    volumeCodes = [NSMutableArray new];
    
    for (NSDictionary *dict in self.requestItem.volumes)
    {
        
        @try {
            NSString *label = [[dict allKeys] objectAtIndex:0];
            [volumeLabels addObject:label];
            [volumeCodes addObject:[dict objectForKey:label]];
        }@catch (NSException *e)
        {
            [e callStackSymbols];
        }
        
       

    }
    
    NSDictionary *pickupLocations = [configLibraryInformation objectForKey:@"pickupLocations"];
    
    
    pickupLocationLabels = [[pickupLocations allKeys] sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)];
    pickupLocationCodes = [pickupLocations objectsForKeys: pickupLocationLabels notFoundMarker: [NSNull null]];
    NSLog(@"%@ %@ %@", pickupLocations, pickupLocationLabels, pickupLocationCodes);
    NSLog(@"There are %zd accounts.", [accountManager numberOfAccounts]);
    
    
    //If no accounts, we must ask for info.
    if ([accountManager numberOfAccounts] == 0)
    {
        self.txtBarcode.hidden = NO;
        self.txtPassword.hidden = NO;
        self.pvAccount.hidden = YES;
    }else{
        self.txtBarcode.hidden = YES;
        self.txtPassword.hidden = YES;
        self.pvAccount.hidden = NO;
    }
    
    if ([accountManager numberOfAccounts] > 0)
    {
        @try {
            CTPatronAccount *account = [[accountManager getAllStoredAccounts] objectAtIndex:0];
            NSLog(@"%@", account.homeLibrary);
            [_pvLocation selectRow:[pickupLocationCodes indexOfObject:account.homeLibrary] inComponent:0 animated:NO];
        }@catch (NSException *e)
        {
            [_pvLocation selectRow:0 inComponent:0 animated:NO];
        }
        
    }
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [FIRAnalytics logEventWithName:kFIREventScreenView parameters:@{
        kFIRParameterScreenName: @"Request Item",
        kFIRParameterScreenClass: @"Request Item"
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView == _pvAccount)
    {
        CTPatronAccount *account = [[accountManager getAllStoredAccounts] objectAtIndex:row];
        @try {
            [_pvLocation selectRow:[pickupLocationCodes indexOfObject:account.homeLibrary] inComponent:0 animated:NO];
        } @catch (NSException *e)
        {
            [_pvLocation selectRow:0 inComponent:0 animated:NO];
        }
    }
}


// The number of columns of data
- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView == _pvAccount)
    {
        return (int)[accountManager numberOfAccounts];
    }else if (pickerView == _pvLocation)
    {
        return (int)[pickupLocationLabels count];
    }else if (pickerView == _pvVolume)
    {
        if ([self.requestItem.volumes count] == 0)
        {
            return 1;
        }else{
            return (int)[volumeLabels count];
        }
    }
    
    return 0;
    
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (pickerView == _pvAccount)
    {
        CTPatronAccount *account = [[accountManager getAllStoredAccounts] objectAtIndex:row];
        return account.name;
    }else if (pickerView == _pvLocation)
    {
        return pickupLocationLabels[row];
    }else if (pickerView == _pvVolume)
    {
        if ([self.requestItem.volumes count] == 0)
        {
            return @"First Available Copy";
        }
        else{
            NSLog(@"Name: %@", [volumeLabels objectAtIndex:row]);
            return [volumeLabels objectAtIndex:row];
        }
    }
    
    return @"";
    
}

- (IBAction)requestItem:(id)sender
{
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Requesting..."];
    /*
    NSString *volumeCode = [volumeCodes objectAtIndex:[_pvVolume selectedRowInComponent:0]];
    
    
    [accountManager  requestItem:self.requestItem.recordId
                      forAccount:[[accountManager getAllStoredAccounts] objectAtIndex:[_pvAccount selectedRowInComponent:0]]
                  pickupLocation:[pickupLocationCodes objectAtIndex:[_pvLocation selectedRowInComponent:0]]
                          volume: [volumeCode stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];*/
    
      [FIRAnalytics logEventWithName:@"item_request"
            parameters:@{
                         @"item_id":self.requestItem.recordId,
                         @"pickup_location":[pickupLocationCodes objectAtIndex:[_pvLocation selectedRowInComponent:0]]

                         }];
    
    [accountManager  requestItem:self.requestItem.recordId
        forAccount:[[accountManager getAllStoredAccounts] objectAtIndex:[_pvAccount selectedRowInComponent:0]]
    pickupLocation:[pickupLocationCodes objectAtIndex:[_pvLocation selectedRowInComponent:0]]
                          volume: @""];
    
}

- (void)itemRequestSuccessful:(BOOL)success responseMessage:(NSString *)message forAccount:(CTPatronAccount *)account
{
    [DejalBezelActivityView removeViewAnimated:YES];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Hold Request"
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:@"Close"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * _Nonnull action) {
                                       if (success)
                                       {
                                           [self dismissViewControllerAnimated:YES completion:nil];
                                       }
                                   }];
    
    
    [alertController addAction:cancelAction];
    
    UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
    alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
    alertPopoverPresentationController.sourceView = self.view;
    
    [self presentViewController:alertController animated:YES completion:nil];
}


@end

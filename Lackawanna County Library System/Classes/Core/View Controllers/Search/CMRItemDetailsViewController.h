//
//  CMRItemDetailsViewController.h
//  CapiraMobile Ready
//
//  Created by Michael on 5/7/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import "CMRViewController.h"
#import "CMRAccountSetupViewController.h"
#import <CTSearchManager/CTSearchManager.h>
#import <WebKit/WebKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface CMRItemDetailsViewController : CMRViewController <CTCatalogServicesDelegate, CTAccountSetupDelegate, UITableViewDelegate, UITableViewDataSource, WKUIDelegate>

@property (nonatomic, weak) IBOutlet WKWebView *webView;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic) CTSearchResult *displayItem;

@end

NS_ASSUME_NONNULL_END

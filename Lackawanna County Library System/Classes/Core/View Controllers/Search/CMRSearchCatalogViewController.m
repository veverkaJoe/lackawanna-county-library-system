//
//  CMRSearchCatalogViewController.m
//  CapiraMobile Ready
//
//  Created by Michael on 5/7/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import "CMRSearchCatalogViewController.h"
#import "CMRItemDetailsViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
@import Firebase;

@interface CMRSearchCatalogViewController ()
{
    NSMutableArray *listOfResults;
    BOOL isFirstLoad;
}
@end

@implementation CMRSearchCatalogViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    isFirstLoad = YES;
    listOfResults = [NSMutableArray new];
    
    self.tableView.alpha = 0.0;
    
    
    //Config Search Bar
    self.searchBar.placeholder = NSLocalizedString(@"Enter Search Terms", "");
    self.topNavBar.topItem.title = NSLocalizedString(@"Search the Catalog","");

    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"13.0"))
    {
        //Bug fix for iOS 13 and Search Bars
        self.searchBar.searchTextField.backgroundColor = [UIColor whiteColor];
    }
    
    if(self.searchParamsDict != nil){
           [self handleSearch:self.searchBar];
            self.searchBar.text = [self.searchParamsDict objectForKey:@"query"];
    }
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([_quickSearchTerm length] > 0)
    {
        if(isFirstLoad)
        {
            isFirstLoad = NO;
            self.searchBar.text = _quickSearchTerm;
            [self handleSearch:self.searchBar];
        }
    }
    [FIRAnalytics logEventWithName:kFIREventScreenView parameters:@{
        kFIRParameterScreenName: @"Search The Catalog",
        kFIRParameterScreenClass: @"Search The Catalog"
    }];

    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark UISEARCHBAR_DELEGATES

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.searchBar setShowsCancelButton:NO animated:YES];
    [self handleSearch:self.searchBar];
}

- (void)handleSearch:(UISearchBar *)searchBar {
   
    NSLog(@"User searched for %@", searchBar.text);
    if((([self.searchBar.text length] < 1) || ([self.searchBar.text isEqualToString:@" "])) && (self.searchParamsDict == nil))
    {
        self.searchBar.text = @"";
        [self.searchBar resignFirstResponder];
        return;
    }
    [self.searchBar resignFirstResponder]; // if you want the keyboard to go away
   
    //Scroll the tableview to the top on subsequent searches
    @try {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:true];
    } @catch (NSException *exception) {
        NSLog(@"No scrolling available");
    }

    
    [UIView animateWithDuration:0.4
                          delay:0
                        options:UIViewAnimationOptionTransitionNone
                     animations:^{
                         
                         self.tableView.alpha = 0.0;
                         [DejalBezelActivityView activityViewForView:self.view];
                         // other animations here
                     }
                     completion:^(BOOL finished){
        CTCatalogServices *webService = [[CTCatalogServices alloc] initWithVersion:@"v1" libraryCode:[self->configLibraryInformation objectForKey:@"code"]];
                         
                         webService.delegate = self;
                        [FIRAnalytics logEventWithName:@"catalog_search"
                            parameters:@{
                                @"search_query": searchBar.text
                            }];
        
                            if(self->isFirstLoad){
                                
                                //[_searchParamsDict setObject:self.searchMaterial forKey:@"material"];
                                [webService executeSearchWithQuery:searchBar.text customParameters:self.searchParamsDict];
                                self->isFirstLoad = false;
                                [webService executeSearchWithQuery:searchBar.text materialCode:@"" libraryScope:@""];
                            }else{
                                [webService executeSearchWithQuery:searchBar.text materialCode:@"" libraryScope:@""];
                            }
        
                         
                         
                    }
     ];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    self.searchBar.text = @"";
}

#pragma mark -
#pragma mark WEBSERVICES_DELEGATE
- (void)didFailWithError:(NSString *)errorMsg httpStatusCode:(NSInteger)statusCode
{
    [DejalBezelActivityView removeViewAnimated:YES];
}

- (void)didReceiveResults:(NSArray *)searchResults
{
    [DejalBezelActivityView removeViewAnimated:YES];
    
    listOfResults = [searchResults mutableCopy];
    
    if ([listOfResults count] == 0)
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(@"No Results", "")
                                              message:NSLocalizedString(@"Your search query returned no results","")
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"Close","")
                                       style:UIAlertActionStyleCancel
                                       handler:nil];
        
        
        [alertController addAction:cancelAction];
        
        UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
        alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
        alertPopoverPresentationController.sourceView = self.view;
        
        [self presentViewController:alertController animated:YES completion:nil];
    }else{
        [self.tableView reloadData];
        [UIView animateWithDuration:0.4
                              delay:0
                            options:UIViewAnimationOptionTransitionNone
                         animations:^{
                             self.tableView.alpha = 1.0;
                             // other animations here
                         }
                         completion:^(BOOL finished){
                                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                                                      [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
                         }
         ];
    }
}


- (BOOL)textFieldShouldClear:(UITextField *)textField {
    return YES;
}

#pragma mark -
#pragma mark UITABLEVIEW_DELEGATES

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CMRItemDetailsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"Item Details"];
    vc.displayItem = [listOfResults objectAtIndex: indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    CMRItemDetailsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"Item Details"];
    vc.displayItem = [listOfResults objectAtIndex: indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [listOfResults count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? 156 : 160;
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"CellResult";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    CTSearchResult *sr = [listOfResults objectAtIndex:indexPath.row];
    
    UILabel *title          = (UILabel *)[cell viewWithTag:100];
    UILabel *material       = (UILabel *)[cell viewWithTag:101];
    UILabel *authorpub       = (UILabel *)[cell viewWithTag:102];
    UILabel *publish        = (UILabel *)[cell viewWithTag:104];
    UIImageView *coverImage = (UIImageView *)[cell viewWithTag:103];
    
    title.text = sr.title;
    material.text = sr.material;
    authorpub.text = sr.author;
    publish.text = sr.publisher;
    

    NSLog(@"Cover %@", sr.coverImage);
    
    [coverImage sd_setImageWithURL:[NSURL URLWithString:sr.coverImage]
                      placeholderImage:[UIImage imageNamed:@"defaultcover.png"]
                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                 if (coverImage.image.size.width < 20.0)
                                 {
                                     [coverImage setImage:[UIImage imageNamed:@"defaultcover.png"]];
                                 }
                             }];
    
    
    
    return cell;

}


@end

//
//  CMRSearchCatalogViewController.h
//  CapiraMobile Ready
//
//  Created by Michael on 5/7/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import "CMRViewController.h"
#import <CTSearchManager/CTSearchManager.h>

NS_ASSUME_NONNULL_BEGIN

@interface CMRSearchCatalogViewController : CMRViewController <CTCatalogServicesDelegate, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSString *quickSearchTerm;
@property (nonatomic, strong) NSMutableDictionary *searchParamsDict;

@end

NS_ASSUME_NONNULL_END

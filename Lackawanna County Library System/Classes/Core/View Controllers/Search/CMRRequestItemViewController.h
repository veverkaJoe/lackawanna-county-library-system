//
//  CMRRequestItemViewController.h
//  CapiraMobile Ready
//
//  Created by Michael on 5/8/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import "CMRViewController.h"
#import <CTSearchManager/CTSearchManager.h>
NS_ASSUME_NONNULL_BEGIN

@interface CMRRequestItemViewController : CMRViewController<CTAccountsManagerDelegate>

- (IBAction)requestItem:(id)sender;


@property (nonatomic, weak) IBOutlet UIPickerView *pvAccount;
@property (nonatomic, weak) IBOutlet UIPickerView *pvLocation;
@property (nonatomic, weak) IBOutlet UIPickerView *pvVolume;

@property (nonatomic, weak) IBOutlet UITextField *txtBarcode;
@property (nonatomic, weak) IBOutlet UITextField *txtPassword;
@property (nonatomic, strong) CTSearchResult *requestItem;
@end

NS_ASSUME_NONNULL_END

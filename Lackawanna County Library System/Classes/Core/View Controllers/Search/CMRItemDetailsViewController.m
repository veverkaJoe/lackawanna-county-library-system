//
//  CMRItemDetailsViewController.m
//  CapiraMobile Ready
//
//  Created by Michael on 5/7/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import "CMRItemDetailsViewController.h"
#import "CMRRequestItemViewController.h"
#define kTitleLabelHeight   75
#define kCellRequest        @"CellRequest"
#define kCellViewOnline     @"CellViewOnline"
#define kCellItemDetails    @"CellItemDetails"
@import Firebase;

@interface CMRItemDetailsViewController ()
{
    NSString *webViewHTML;
    NSMutableArray *expandedTableCells;
    NSMutableArray *cellIdentifiers;
    CGFloat webViewHeight;
    
}
@end

@implementation CMRItemDetailsViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.topNavBar.topItem.title = self.displayItem.title;
    
    expandedTableCells  = [NSMutableArray new];
    cellIdentifiers     = [NSMutableArray new];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        [self.tableView setScrollEnabled:YES];
    }
    
    
    [self.tableView setBounces:NO];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    
    CTCatalogServices *webService = [[CTCatalogServices alloc] initWithVersion:@"v1" libraryCode:[configLibraryInformation objectForKey:@"code"]];
    
    webService.delegate = self;
    
    [webService retrieveItemDetails:self.displayItem.recordId];
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:NSLocalizedString(@"Loading...", "")];
    
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:0 inSection:0];
    
    [expandedTableCells addObject:indexPath];
    
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [FIRAnalytics logEventWithName:kFIREventScreenView parameters:@{
        kFIRParameterScreenName: @"Item Details",
        kFIRParameterScreenClass: @"Item Details"
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didReceiveItemDetails:(NSString *)displayHTML
{
    NSLog(@"Item Details Called");
    webViewHTML = displayHTML;
    [self.tableView reloadData];
    [DejalBezelActivityView removeViewAnimated:YES];
}

- (void) didFailWithError:(NSString *)errorMsg httpStatusCode:(NSInteger)statusCode
{
    NSLog(@"Fail Called");
    [DejalBezelActivityView removeViewAnimated:YES];
}

- (void)didUpdateAccounts
{
    CMRRequestItemViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"Request Item"];
    vc.requestItem = self.displayItem;
    vc.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:vc animated:YES completion:nil];
}

#pragma mark -
#pragma mark UITABLEVIEW_DELEGATES
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger totalRows = 1;
    [cellIdentifiers addObject:kCellItemDetails];
    
    //Do we allow holds?
    BOOL allowHolds = ([configLibraryInformation objectForKey:@"allowsHolds"] == nil) ? YES : [[configLibraryInformation objectForKey:@"allowsHolds"] boolValue];
    
    if (self.displayItem.isRequestable && allowHolds)
    {
        totalRows++;
        [cellIdentifiers addObject:kCellRequest];
    }
    
    if (self.displayItem.isDigital)
    {
        totalRows++;
        [cellIdentifiers addObject:kCellViewOnline];
    }
    
    return totalRows;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Request or View Cells? They don't expand
    if ([[cellIdentifiers objectAtIndex:indexPath.row] isEqualToString:kCellViewOnline])
    {
        [self displayBrowser:self.displayItem.link];
        return;
    }
    
    if ([[cellIdentifiers objectAtIndex:indexPath.row] isEqualToString:kCellRequest])
    {
        
        if ([accountManager numberOfAccounts] == 0)
        {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:NSLocalizedString(@"No Accounts Configured", "")
                                                  message:NSLocalizedString(@"Please add your library card in My Account > Manage Accounts.", "")
                                                  preferredStyle:UIAlertControllerStyleActionSheet];
            
            //Add Cancel Button
            UIAlertAction *cancelAction = [UIAlertAction
                                           actionWithTitle:NSLocalizedString(@"Close", "")
                                           style:UIAlertActionStyleCancel
                                           handler:nil];
            
            [alertController addAction:cancelAction];
            
            UIAlertAction *addAction = [UIAlertAction
                                           actionWithTitle:NSLocalizedString(@"Add Account", "")
                                           style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * _Nonnull action) {
                                            CMRAccountSetupViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"Account Setup"];
                                            vc.delegate = self;
                                            vc.modalPresentationStyle = UIModalPresentationFormSheet;
                                            
                                            [self presentViewController:vc animated:YES completion:^{
                                                
                                            }];

                                        }];
            
            
            [alertController addAction:cancelAction];
            [alertController addAction:addAction];
            
            UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
            alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
            alertPopoverPresentationController.sourceView = self.view;
            
            [self presentViewController:alertController animated:YES completion:nil];
            return;
        }
        
        CMRRequestItemViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"Request Item"];
        vc.requestItem = self.displayItem;
        vc.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentViewController:vc animated:YES completion:nil];
        
        return;
    }
    
    if ([expandedTableCells containsObject:indexPath])
    {
        [expandedTableCells removeObject:indexPath];
    }
    else
    {
        [expandedTableCells addObject:indexPath];
    }
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
    
   // [self.tableView reloadData];//
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([expandedTableCells containsObject:indexPath])
    {
       // return  webViewHeight + kTitleLabelHeight; //It's not necessary a constant, though
        return _tableView.frame.size.height - kTitleLabelHeight;
    }
    else
    {
        return kTitleLabelHeight; //Again not necessary a constant
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *MyIdentifier = [cellIdentifiers objectAtIndex:indexPath.row];

    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    
   
    UIImageView *imgArrow = (UIImageView *)[cell viewWithTag:2];
    
    if ([MyIdentifier isEqualToString:kCellViewOnline] || [MyIdentifier isEqualToString:kCellRequest])
    {
        imgArrow.image = [UIImage imageNamed:@"cell_arrowleft_dark.png"];
    }else{
        if ([expandedTableCells containsObject:indexPath])
        {
            imgArrow.image = [UIImage imageNamed:@"cell_arrowup_dark.png"];
        }else{
            imgArrow.image = [UIImage imageNamed:@"cell_arrowdown_dark.png"];
        }
    }
    
    
    
    switch (indexPath.row)
    {
        case 0:
            {
                WKWebView *webView    = (WKWebView *)[cell viewWithTag:1];
                webView.scrollView.scrollEnabled = YES;
                webView.scrollView.bounces = NO;
                [webView loadHTMLString:webViewHTML baseURL:nil];
                
            }
            break;
    }

    
    
    return cell;
}

#pragma mark -
#pragma mark UIWEBVIEW_DELEGATES
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"%lf", webView.scrollView.contentSize.height);
    
    
    //Change in height?
    if (webViewHeight+10 < webView.scrollView.contentSize.height)
    {
        webViewHeight = webView.scrollView.contentSize.height;
      //  [self.tableView reloadData];
    }
    
}

@end

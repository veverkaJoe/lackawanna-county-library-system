//
//  CTBarcodeScannerViewController.h
//  KCKPL
//
//  Created by Michael Berse on 2/26/17.
//  Copyright © 2017 Michael Berse. All rights reserved.
//

#import "CMRViewController.h"
#import <ZXingObjC/ZXingObjC.h>

@protocol CTBarcodeScannerDelegate <NSObject>

@required
-(void)didScanBarcode:(NSString *)barcode;
@end

@interface CTBarcodeScannerViewController : CMRViewController <ZXCaptureDelegate>

@property (nonatomic) id delegate;

@end

//
//  CMRAccountListingViewController.m
//  CapiraMobile Ready
//
//  Created by Michael on 4/18/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import "CMRAccountListingViewController.h"
@import Firebase;

@interface CMRAccountListingViewController ()

@end

@implementation CMRAccountListingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
    self.tableView.allowsSelection = NO;
    
    if ([accountManager numberOfAccounts] == 0)
    {
        self.tableView.hidden     = YES;
        self.lblNoAccounts.hidden = NO;
    }else{
        self.tableView.hidden     = NO;
        self.lblNoAccounts.hidden = YES;
    }
    
    self.topNavBar.topItem.title = NSLocalizedString(@"Accounts", "");
}

- (void)didUpdateAccounts
{
    accountManager.delegate = self;
 
    [self.tableView reloadData];
    
    if ([accountManager numberOfAccounts] == 0)
    {
        self.tableView.hidden     = YES;
        self.lblNoAccounts.hidden = NO;
    }else{
        self.tableView.hidden     = NO;
        self.lblNoAccounts.hidden = YES;
    }

    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [FIRAnalytics logEventWithName:kFIREventScreenView parameters:@{
        kFIRParameterScreenName: @"Accounts",
        kFIRParameterScreenClass: @"Accounts"
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addAccount:(id)sender
{
    [FIRAnalytics logEventWithName:@"user_interaction"
        parameters:@{@"name": @"Add Account"
    }];
    
    CMRAccountSetupViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"Account Setup"];
    vc.delegate = self;
    vc.modalPresentationStyle = UIModalPresentationFormSheet;
    
    [self presentViewController:vc animated:YES completion:^{
        [self.tableView reloadData];
    }];
}

- (IBAction)deleteAccount:(id)sender
{
    [FIRAnalytics logEventWithName:@"user_interaction"
        parameters:@{@"name": @"Delete Account"
    }];
    
    UIButton *button = (UIButton *)sender;
    
    CTPatronAccount *userAccount = [[accountManager getAllStoredAccounts] objectAtIndex:button.tag];
    
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Are you sure?"
                                          message:[NSString stringWithFormat:@"Do you want to remove the account for %@ - %@", userAccount.name, userAccount.barcode]
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * _Nonnull action) {
                                      
                                   }];
    
    
    UIAlertAction *removeAction = [UIAlertAction
                                   actionWithTitle:@"Remove"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * _Nonnull action) {
                                       [self->accountManager removeAccountWithBarcode:userAccount.barcode];
                                       [self.tableView reloadData];
                                        
                                       if ([self->accountManager numberOfAccounts] == 0)
                                       {
                                           self.tableView.hidden     = YES;
                                           self.lblNoAccounts.hidden = NO;
                                       }else{
                                           self.tableView.hidden     = NO;
                                           self.lblNoAccounts.hidden = YES;
                                       }

                                   }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:removeAction];
    
    UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
    alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
    alertPopoverPresentationController.sourceView = self.view;
    
    [self presentViewController:alertController animated:YES completion:^{
        
    }];

}

#pragma mark -
#pragma mark UITABLEVIEW_DELEGATES
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [accountManager numberOfAccounts];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"AccountCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    
    UILabel *lblPatronName           = (UILabel *)[cell viewWithTag:100];
    UILabel *lblPatronBarcode        = (UILabel *)[cell viewWithTag:101];
    UILabel *lblPatronHome           = (UILabel *)[cell viewWithTag:102];
    UILabel *lblPatronExpiration     = (UILabel *)[cell viewWithTag:103];

    UIButton *btnRemove              = (UIButton *)[cell viewWithTag:104];
    
    CTPatronAccount *account = [[accountManager getAllStoredAccounts] objectAtIndex:indexPath.row];
    NSLog(@"Account : %@",account);

    NSDictionary *pickupLocations = [configLibraryInformation objectForKey:@"pickupLocations"];
    
    NSArray *pickupLocationLabels;
    NSArray *pickupLocationCodes;
    pickupLocationLabels = [[pickupLocations allKeys] sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)];
    pickupLocationCodes = [pickupLocations objectsForKeys: pickupLocationLabels notFoundMarker: [NSNull null]];
    NSLog(@"%@ %@ %@", pickupLocations, pickupLocationLabels, pickupLocationCodes);
    
    NSString *translatedLibraryCode = account.homeLibrary;
    
    @try {
      for(int i = 0; i< pickupLocationCodes.count; i++){
          if([pickupLocationCodes[i] isEqualToString:account.homeLibrary]){
              translatedLibraryCode = pickupLocationLabels[i];
          }
      }
    } @catch (NSException *exception) {
        NSLog(@"%@ ",exception.name);
        NSLog(@"Reason: %@ ",exception.reason);
        translatedLibraryCode = account.homeLibrary;
    }
    
    
    lblPatronName.text          = account.name;
    lblPatronBarcode.text       = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Barcode", ""), account.barcode];
    lblPatronHome.text          = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Registered", ""), translatedLibraryCode];
    lblPatronExpiration.text    = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Expires", ""), account.expirationDate];
    
    //Set Button Tag
    btnRemove.tag = indexPath.row;
       
    return cell;
}



@end

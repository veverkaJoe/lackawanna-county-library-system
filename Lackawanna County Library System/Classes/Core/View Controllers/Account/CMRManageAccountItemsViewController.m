//
//  CTManageAccountItemsViewController.m
//  KCKPL
//
//  Created by Michael Berse on 2/7/17.
//  Copyright © 2017 Michael Berse. All rights reserved.
//

#import "CMRManageAccountItemsViewController.h"
@import Firebase;

@interface CMRManageAccountItemsViewController ()
{
    NSMutableArray *listOfCheckouts;
    NSMutableArray *listOfHolds;
    NSMutableArray *listOfFines;
    
    BOOL isFirstLoad;
}
@end

@implementation CMRManageAccountItemsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.topNavBar.topItem.title = NSLocalizedString(@"View My Record", "");
    
    //Init Empty Arrays
    listOfCheckouts = [NSMutableArray new];
    listOfHolds = [NSMutableArray new];
    listOfFines = [NSMutableArray new];
    
    self.tableView.hidden = YES;
    self.lblFines.hidden = YES;
    self.btnManage.hidden = YES;
    
    isFirstLoad = YES;
    
    if (self.requestType == kAccountItemsFines)
    {
        self.tableView.allowsSelection = NO;
        [self.btnManage setTitle:@"PAY FINES" forState:UIControlStateNormal];
        [self.btnManage removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
        [self.btnManage addTarget:self action:@selector(showFinesPage) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if (self.requestType == kAccountItemsReadingHistory)
    {
        self.tableView.allowsSelection = NO;
        [self.btnManage setHidden:YES];
        [self.btnManage removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    }
    
    if (self.requestType == kAccountItemsCheckouts)
    {
        [self.btnManage setTitle:NSLocalizedString(@"RENEW SELECTED", "") forState:UIControlStateNormal];
    }
    
    if (self.requestType == kAccountItemsHolds){
    }
}



- (void)showFinesPage
{
    [self displayBrowser:@"https://catalog.ccls.org/patroninfo"];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (isFirstLoad)
    {
        [self getAccountItems];
    }
    [FIRAnalytics logEventWithName:kFIREventScreenView parameters:@{
        kFIRParameterScreenName: @"View My Record",
        kFIRParameterScreenClass: @"View My Record"
    }];
}


- (void)getAccountItems
{
    [DejalBezelActivityView activityViewForView:self.view withLabel:NSLocalizedString(@"Loading...", "")];
    
    if (self.requestType == kAccountItemsHolds)
    {
        [accountManager getHolds:self.chosenPatron];
    }else if (self.requestType == kAccountItemsCheckouts){
        [accountManager getCheckouts:self.chosenPatron];
    }else if (self.requestType == kAccountItemsReadingHistory){
        [accountManager getReadingHistory:self.chosenPatron];
    }else{
        [accountManager getFines:self.chosenPatron];
    }
}

- (void)cancelHolds
{

    [FIRAnalytics logEventWithName:@"user_interaction"
        parameters:@{@"name": @"Cancel Items"
    }];
    NSMutableArray *itemIds = [NSMutableArray new];
    
    //Get Selected Rows
    NSArray *selectedIndexPaths = [self.tableView indexPathsForSelectedRows];
    
    //Any checkouts selected?
    if ([selectedIndexPaths count] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Holds Selected" message:@"You must select items from your Holds list." delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    //Get holds, combine item ids with comma.
    for (NSIndexPath *indexPath in selectedIndexPaths)
    {
        CTHoldItem *temp = [listOfHolds objectAtIndex:indexPath.row];
        [itemIds addObject:temp.recordId];
    }
    
    NSLog(@"Item IDS: %@", itemIds);

    [DejalBezelActivityView activityViewForView:self.view withLabel:NSLocalizedString(@"Loading...", "")];
    
    [accountManager manageItems:itemIds forAccount:self.chosenPatron actionType:kAccountItemActionCancel];
}

- (void)freezeHolds
{
    [FIRAnalytics logEventWithName:@"user_interaction"
        parameters:@{@"name": @"Freeze Items"
    }];
    NSMutableArray *itemIds = [NSMutableArray new];
    
    //Get Selected Rows
    NSArray *selectedIndexPaths = [self.tableView indexPathsForSelectedRows];
    
    //Any checkouts selected?
    if ([selectedIndexPaths count] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Holds Selected" message:@"You must select items from your Holds list." delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    //Get holds, combine item ids with comma.
    for (NSIndexPath *indexPath in selectedIndexPaths)
    {
        CTHoldItem *temp = [listOfHolds objectAtIndex:indexPath.row];
        [itemIds addObject:temp.recordId];
    }
    
    NSLog(@"Item IDS: %@", itemIds);
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:NSLocalizedString(@"Loading...", "")];
    
    [accountManager manageItems:itemIds forAccount:self.chosenPatron actionType:kAccountItemActionFreeze];
}

- (void)unfreezeHolds
{
    [FIRAnalytics logEventWithName:@"user_interaction"
        parameters:@{@"name": @"Unfreeze Items"
    }];
    NSMutableArray *itemIds = [NSMutableArray new];
    
    //Get Selected Rows
    NSArray *selectedIndexPaths = [self.tableView indexPathsForSelectedRows];
    
    //Any checkouts selected?
    if ([selectedIndexPaths count] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Holds Selected" message:@"You must select items from your Holds list." delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    //Get holds, combine item ids with comma.
    for (NSIndexPath *indexPath in selectedIndexPaths)
    {
        CTHoldItem *temp = [listOfHolds objectAtIndex:indexPath.row];
        [itemIds addObject:temp.recordId];
    }
    
    NSLog(@"Item IDS: %@", itemIds);
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:NSLocalizedString(@"Loading...", "")];
    
    [accountManager manageItems:itemIds forAccount:self.chosenPatron actionType:kAccountItemActionUnfreeze];
}

- (void)renewItems
{
    [FIRAnalytics logEventWithName:@"user_interaction"
        parameters:@{@"name": @"Renew Items"
    }];
    
    NSMutableArray *itemIds = [NSMutableArray new];
    
    //Get Selected Rows
    NSArray *selectedIndexPaths = [self.tableView indexPathsForSelectedRows];
    
    //Any checkouts selected?
    if ([selectedIndexPaths count] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Items Selected" message:@"You must select items from your Checkouts list." delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    //Get holds, combine item ids with comma.
    for (NSIndexPath *indexPath in selectedIndexPaths)
    {
        CTCheckoutItem *temp = [listOfCheckouts objectAtIndex:indexPath.row];
        [itemIds addObject:temp.recordId];
    }
    
    NSLog(@"Item IDS: %@", itemIds);
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:NSLocalizedString(@"Loading...", "")];
    
    [accountManager manageItems:itemIds forAccount:self.chosenPatron actionType:kAccountItemActionRenew];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)manageSelectedItems:(id)sender
{
    NSArray *selectedIndexPaths = [self.tableView indexPathsForSelectedRows];
    
    if ([selectedIndexPaths count] == 0)
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(@"No Items Selected", "")
                                              message:NSLocalizedString(@"You must select items to manage.", "")
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"Close", "")
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction * _Nonnull action) {
                                       
                                       }];
        
        
        [alertController addAction:cancelAction];
        
        UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
        //alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
        alertPopoverPresentationController.sourceView = self.view;
        
        [self presentViewController:alertController animated:YES completion:^{
            
        }];
    }
    
    NSString *msgTitle = (self.requestType == kAccountItemsCheckouts) ? NSLocalizedString(@"Renew Item(s)", "") : NSLocalizedString(@"Manage Selected Items", "");
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:msgTitle
                                          message:nil
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", "")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * _Nonnull action) {
                                       
                                   }];
    
    UIAlertAction *cancelHoldAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel Hold(s)", "")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * _Nonnull action) {
                                       [self cancelHolds];
                                   }];
    
    UIAlertAction *freezeHoldAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"Freeze Hold(s)", "")
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * _Nonnull action) {
                                           [self freezeHolds];
                                       }];
    
    UIAlertAction *unfreezeHoldAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"Unfreeze Hold(s)", "")
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * _Nonnull action) {
                                           [self unfreezeHolds];
                                       }];
    
    UIAlertAction *renewAction = [UIAlertAction
                                         actionWithTitle:NSLocalizedString(@"Renew Item(s)", "")
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * _Nonnull action) {
                                             [self renewItems];
                                         }];
    
    [alertController addAction:cancelAction];
    
    if (self.requestType == kAccountItemsCheckouts){
        [alertController addAction:renewAction];
    }else{
        [alertController addAction:cancelHoldAction];
        BOOL isHoldDisabled = false;
        @try {
            isHoldDisabled = [configLibraryInformation objectForKey:@"disableHoldSuspension"];
        } @catch (NSException *exception) {
            isHoldDisabled = false;
            NSLog(@"CRASH: %@", exception);
            NSLog(@"Stack Trace: %@", [exception callStackSymbols]);
        }
        if (!isHoldDisabled) {
            [alertController addAction:freezeHoldAction];
            [alertController addAction:unfreezeHoldAction];
        }
    }
    
    UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
    //alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
    alertPopoverPresentationController.sourceView = self.view;
    
    [self presentViewController:alertController animated:YES completion:^{
        
    }];
    
    
}

#pragma mark -
#pragma mark ACCOUNTMANAGER_DELEGATES
- (void)itemActionSuccessful:(BOOL)success responseMessage:(NSString *)message actionType:(AccountItemAction)action
{
    [DejalBezelActivityView removeViewAnimated:YES];
    
    
    NSString *msgTitle = nil;
    if ((self.requestType == kAccountItemsCheckouts) && success)
    {
        msgTitle = NSLocalizedString(@"Renewed", "");
    }else{
        msgTitle = NSLocalizedString(@"Item Update", "");
    }
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:msgTitle
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Close", "")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * _Nonnull action) {
                                       
                                       //Reload data?
                                        if (success)
                                        {
                                            [self getAccountItems];
                                        }
                                   }];
    
    
    [alertController addAction:cancelAction];

    UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
    alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
    alertPopoverPresentationController.sourceView = self.view;
    
    [self presentViewController:alertController animated:YES completion:^{
        
    }];

}
- (void)didReceiveReadingHistory:(NSArray *)readingHistoryItems forAccount:(CTPatronAccount *)account
{
    [DejalBezelActivityView removeViewAnimated:YES];
    listOfCheckouts = [readingHistoryItems mutableCopy];
    [self.tableView reloadData];
    self.lblFines.text = [NSString stringWithFormat:@"%@: $%.02f", NSLocalizedString(@"Fines", ""), account.totalFines];
    self.tableView.hidden = NO;
    self.lblFines.hidden = NO;
}

- (void)didReceiveFines:(NSArray *)fineItems forAccount:(CTPatronAccount *)account
{
    NSLog(@"FINES CALLED");
    self.btnManage.hidden = NO;
    
    [DejalBezelActivityView removeViewAnimated:YES];
    listOfFines = [fineItems mutableCopy];
    [self.tableView reloadData];
    self.lblFines.text = [NSString stringWithFormat:@"%@: $%.02f",NSLocalizedString(@"Fines", ""), account.totalFines];
    self.tableView.hidden = NO;
    self.lblFines.hidden = YES;
}

- (void)didReceiveHolds:(NSArray *)holdItems forAccount:(CTPatronAccount *)account
{
    [DejalBezelActivityView removeViewAnimated:YES];
    listOfHolds = [holdItems mutableCopy];
    [self.tableView reloadData];
    self.lblFines.text = [NSString stringWithFormat:@"%@: $%.02f", NSLocalizedString(@"Fines", ""), account.totalFines];
    self.tableView.hidden = NO;
    self.lblFines.hidden = NO;
    
    if ([holdItems count] > 0)
    {
        self.btnManage.hidden = NO;
    }else{
        self.btnManage.hidden = YES;
    }
}

- (void)didReceiveCheckouts:(NSArray *)checkoutItems forAccount:(CTPatronAccount *)account
{
    [DejalBezelActivityView removeViewAnimated:YES];
    listOfCheckouts = [checkoutItems mutableCopy];
    [self.tableView reloadData];
    self.lblFines.text = [NSString stringWithFormat:@"%@: $%.02f", NSLocalizedString(@"Fines", ""), account.totalFines];
    self.tableView.hidden = NO;
    self.lblFines.hidden = NO;
    
    if ([checkoutItems count] > 0)
    {
        self.btnManage.hidden = NO;
    }else{
        self.btnManage.hidden = YES;
    }
}

- (void)didFailWithError:(NSString *)errorMsg httpStatusCode:(NSInteger)statusCode
{
    [DejalBezelActivityView removeViewAnimated:YES];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Error"
                                          message:errorMsg
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:@"Close"
                                   style:UIAlertActionStyleCancel
                                   handler:nil];
    
    
    [alertController addAction:cancelAction];
    
    UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
    alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
    alertPopoverPresentationController.sourceView = self.view;
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark -
#pragma mark TABLVIEW_DELEGATES

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger arrayCount = 0;

    if (self.requestType == kAccountItemsCheckouts || self.requestType == kAccountItemsReadingHistory)
    {
        arrayCount = [listOfCheckouts count];
    }else if (self.requestType == kAccountItemsHolds){
        arrayCount = [listOfHolds count];
    }else{
        arrayCount = [listOfFines count];
    }
    
    return arrayCount;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{

    NSString *sectionName;
    
    if (self.requestType == kAccountItemsCheckouts)
    {
        sectionName = [NSString stringWithFormat:@"%ld", (long)[listOfCheckouts count]];
    }else if (self.requestType == kAccountItemsHolds) {
        sectionName = [NSString stringWithFormat:@"%ld", (long)[listOfHolds count] ];
    }else if (self.requestType == kAccountItemsReadingHistory) {
        sectionName = [NSString stringWithFormat:@"%ld", (long)[listOfCheckouts count] ];
    }else{
        sectionName = [NSString stringWithFormat:@"%ld", (long)[listOfFines count] ];
    }
    
    //Center section label
    [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setTextAlignment:NSTextAlignmentCenter];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setFont:[UIFont fontWithName:@"Helvetica-Bold" size:28]];
    }else{
        [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
    }
    
    return sectionName;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0)
    {
        return 50.0f;
    }
    
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = nil;
    
    if (self.requestType == kAccountItemsCheckouts || self.requestType == kAccountItemsReadingHistory) {
        cellIdentifier = @"cellCheckout";
    }
    else{
        cellIdentifier = @"cellHold";
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    
    if (self.requestType == kAccountItemsCheckouts || self.requestType == kAccountItemsReadingHistory) {
        UILabel *title = (UILabel *)[cell viewWithTag:1];
        UILabel *duedate = (UILabel *)[cell viewWithTag:2];
        
        
        CTCheckoutItem *item = [listOfCheckouts objectAtIndex:indexPath.row];
        
        //If Item Overdue, highlight red
        if ([item isOverdue])
        {
            cell.backgroundColor = [UIColor colorWithRed:255.0f/255.0f green:145.0f/255.0f blue:156.0f/255.0f alpha:1.0f];
        }else{
            cell.backgroundColor = [UIColor whiteColor];
        }
        
        title.text = item.title;
        if (self.requestType == kAccountItemsReadingHistory)
        {
            duedate.text = item.checkoutDate;
        }else{
            duedate.text = item.dueDate;
        }
        
        NSLog(@"Checkout Cell Called");
    }
    else if (self.requestType == kAccountItemsHolds){
        CTHoldItem *item = [listOfHolds objectAtIndex:indexPath.row];
        UILabel *title = (UILabel *)[cell viewWithTag:1];
        UILabel *status = (UILabel *)[cell viewWithTag:2];
    
        //If Hold is Ready, highlight green
        if ([item isReady])
        {
            cell.backgroundColor = [UIColor colorWithRed:191.0f/255.0f green:255.0f/255.0f blue:196.0f/255.0f alpha:1.0f];
        }
        else if([item isFrozen]) {
            cell.backgroundColor = [UIColor colorWithRed:191.0f/255.0f green:223.0f/255.0f blue:232.0f/255.0f alpha:1.0f];
        }else{
            cell.backgroundColor = [UIColor whiteColor];
        }
        

        title.text = item.title;
        status.text = [NSString stringWithFormat:@"%@ -- %@", item.status, item.pickupLocation];
        
        NSLog(@"Holds Cell Called");
    }else{
        CTFinesItem *item = [listOfFines objectAtIndex:indexPath.row];
        UILabel *title = (UILabel *)[cell viewWithTag:1];
        UILabel *status = (UILabel *)[cell viewWithTag:2];
    
        title.text = item.fineDescription;
        status.text = item.fineAmount;
        
        NSLog(@"fines desc %@", item.fineDescription);
    }
    
    
    return cell;
}


@end

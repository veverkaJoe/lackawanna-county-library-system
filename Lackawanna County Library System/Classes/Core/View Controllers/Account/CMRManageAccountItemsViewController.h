//
//  CTManageAccountItemsViewController.h
//  KCKPL
//
//  Created by Michael Berse on 2/7/17.
//  Copyright © 2017 Michael Berse. All rights reserved.
//

#import "CMRViewController.h"

typedef NS_ENUM(NSUInteger, AccountItemsRequestType) {
    kAccountItemsCheckouts,
    kAccountItemsHolds,
    kAccountItemsFines,
    kAccountItemsReadingHistory
};

@interface CMRManageAccountItemsViewController : CMRViewController <CTAccountsManagerDelegate>

@property (nonatomic) CTPatronAccount *chosenPatron;
@property (nonatomic) AccountItemsRequestType requestType;

@property (nonatomic, weak) IBOutlet UIButton *btnManage;
@property (nonatomic, weak) IBOutlet UILabel *lblFines;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

-(IBAction)manageSelectedItems:(id)sender;
-(void)renewItems;
-(void)cancelHolds;
-(void)freezeHolds;
-(void)unfreezeHolds;
@end

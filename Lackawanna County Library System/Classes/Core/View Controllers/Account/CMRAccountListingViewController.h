//
//  CMRAccountListingViewController.h
//  CapiraMobile Ready
//
//  Created by Michael on 4/18/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import "CMRViewController.h"
#import "CMRAccountSetupViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CMRAccountListingViewController : CMRViewController <CTAccountSetupDelegate, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UILabel     *lblNoAccounts;
- (IBAction)addAccount:(id)sender;
- (IBAction)deleteAccount:(id)sender;

@end

NS_ASSUME_NONNULL_END

//
//  CMRAccountMenuViewController.h
//  CapiraMobile Ready
//
//  Created by Michael on 3/26/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import "CMRViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CMRAccountMenuViewController : CMRViewController

@property (nonatomic, weak) IBOutlet UIButton *btnSelfCheck;

@end

NS_ASSUME_NONNULL_END

//
//  CMRAccountSetupViewController.h
//  CapiraMobile Ready
//
//  Created by Michael on 4/18/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import "CMRViewController.h"

@protocol CTAccountSetupDelegate <NSObject>
@optional
- (void)didUpdateAccounts;
@end

NS_ASSUME_NONNULL_BEGIN

@interface CMRAccountSetupViewController : CMRViewController
<CTAccountsManagerDelegate>

@property (nonatomic, weak) IBOutlet UITextField *txtBarcode;
@property (nonatomic, weak) IBOutlet UITextField *txtPassword;
@property (nonatomic, weak) IBOutlet UIButton    *btnForgotPassword;
@property (nonatomic, weak) id <CTAccountSetupDelegate> delegate;

- (IBAction)onValidation:(id)sender;
- (IBAction)onSkip:(id)sender;
- (IBAction)onForgotPassword:(id)sender;

@end

NS_ASSUME_NONNULL_END

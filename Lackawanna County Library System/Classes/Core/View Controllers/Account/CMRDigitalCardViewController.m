//
//  CMRDigitalCardViewController.m
//  CapiraMobile Ready
//
//  Created by Michael on 4/2/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import "CMRDigitalCardViewController.h"
@import Firebase;

@interface CMRDigitalCardViewController ()

@end

@implementation CMRDigitalCardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     self.topNavBar.topItem.title = NSLocalizedString(@"Digital Library Card", "");

     //If for some reason no account was set, default to first one
     if (self.chosenPatronAccount == nil)
     {
         self.chosenPatronAccount = [[accountManager getAllStoredAccounts] objectAtIndex:0];
     }
     
     //Set Image
     self.imgLibraryCard.image = [UIImage imageNamed:@"default_card.png"];
     
     
     //Configure Swipe Gesture Recognizers
     UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSwipeGesture:)];
     UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSwipeGesture:)];
     swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
     swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
     
     [self.view addGestureRecognizer:swipeLeft];
     [self.view addGestureRecognizer:swipeRight];
     
     //Layout card barcode and data
    
    [self layoutDigitalCard];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [FIRAnalytics logEventWithName:kFIREventScreenView parameters:@{
        kFIRParameterScreenName: @"Digital Library Card",
        kFIRParameterScreenClass: @"Digital Library Card"
    }];
}


#pragma mark - Private Methods

- (void)layoutDigitalCard
{
    ZXMultiFormatWriter *writer = [[ZXMultiFormatWriter alloc] init];
    ZXBitMatrix *result = [writer encode:self.chosenPatronAccount.barcode
                                  format:[self stringToBarcodeFormat:[configLibraryInformation objectForKey:@"digitalBarcodeFormat"]]
                                   width:self.imgBarcode.frame.size.width
                                  height:self.imgBarcode.frame.size.height
                                   error:nil];
    
    NSLog(@"PASSING BARCODE TO ENCODE: %@", self.chosenPatronAccount.barcode);
    
    if (result) {
        ZXImage *image = [ZXImage imageWithMatrix:result];
        self.imgBarcode.image = [UIImage imageWithCGImage:image.cgimage];
    } else {
        self.imgBarcode.image = nil;
    }
    
    
    self.lblPatronName.text = self.chosenPatronAccount.name;
    self.lblPatronBarcode.text = self.chosenPatronAccount.barcode;
    
    if ([self.chosenPatronAccount.expirationFormattedDate length] == 0)
    {
        self.lblPatronExpiration.text    = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Expires", ""), self.chosenPatronAccount.expirationDate];
    }else{
        self.lblPatronExpiration.text    = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Expires", ""), self.chosenPatronAccount.expirationFormattedDate];
    }

}

- (void) onSwipeGesture:(UISwipeGestureRecognizer *)gestureRecognizer
{
    //Only 1 account? Swiper no swiping.
    if ([accountManager numberOfAccounts] <= 1)
    {
        return;
    }
    
    NSInteger idx = [[accountManager getAllStoredAccounts] indexOfObject:self.chosenPatronAccount];
    
    if (gestureRecognizer.direction == UISwipeGestureRecognizerDirectionLeft)
    {
        //If this is the last account in the array, move to the first account. Otherwise, get the next account in array position
        if (idx == ([accountManager numberOfAccounts] - 1))
        {
            self.chosenPatronAccount = [[accountManager getAllStoredAccounts] objectAtIndex:0];
        }else{
            self.chosenPatronAccount = [[accountManager getAllStoredAccounts] objectAtIndex:idx+1];
        }
    }else if (gestureRecognizer.direction == UISwipeGestureRecognizerDirectionRight)
    {
       //If this is the first account in the array, move to the last account in the array. Otherwise, get the previous account in array position.
       if (idx == 0)
       {
           self.chosenPatronAccount = [[accountManager getAllStoredAccounts] objectAtIndex:[accountManager numberOfAccounts] - 1];
       }else{
           self.chosenPatronAccount = [[accountManager getAllStoredAccounts] objectAtIndex:idx-1];
       }
    }
    
    [self layoutDigitalCard];

}

- (ZXBarcodeFormat)stringToBarcodeFormat:(NSString *)format {
    
    
    NSMutableDictionary *formatDict = [NSMutableDictionary new];
    
    [formatDict setObject:[NSNumber numberWithInt:kBarcodeFormatCodabar]    forKey:@"codabar"];
    [formatDict setObject:[NSNumber numberWithInt:kBarcodeFormatCode39]     forKey:@"code39"];
    [formatDict setObject:[NSNumber numberWithInt:kBarcodeFormatCode93]     forKey:@"code93"];
    [formatDict setObject:[NSNumber numberWithInt:kBarcodeFormatCode128]    forKey:@"code128"];
    [formatDict setObject:[NSNumber numberWithInt:kBarcodeFormatQRCode]     forKey:@"qrcode"];
    
    ZXBarcodeFormat chosenFormat = [[formatDict objectForKey:[format lowercaseString]] intValue];
    
    if (!chosenFormat)
    {
        chosenFormat = kBarcodeFormatCodabar;
    }
    
    return chosenFormat;
}

@end

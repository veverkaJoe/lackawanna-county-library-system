//
//  CMRAccountSetupViewController.m
//  CapiraMobile Ready
//
//  Created by Michael on 4/18/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import "CMRAccountSetupViewController.h"
@import Firebase;

@interface CMRAccountSetupViewController ()

@end

@implementation CMRAccountSetupViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Worthing
    //self.txtBarcode.text = @"2254378314";
    //self.txtPassword.text = @"0123";
   
    //Gail
    //self.txtBarcode.text = @"21113005156514";
    //self.txtPassword.text = @"6514";
    
    //Moorestown
    //self.txtBarcode.text = @"22030000605684";
    //self.txtPassword.text = @"5684";
    
    //Fayette
   //self.txtBarcode.text = @"29850004749318";
   //self.txtPassword.text = @"6374";
    
    //MLCSTL
   //self.txtBarcode.text = @"21300001234567";
   //self.txtPassword.text = @"1234";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"HasLaunchedOnce"] && [accountManager numberOfAccounts] == 0)
    {
        self.topNavBar.topItem.leftBarButtonItem = nil;
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HasLaunchedOnce"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [FIRAnalytics logEventWithName:kFIREventScreenView parameters:@{
        kFIRParameterScreenName: @"Account Setup",
        kFIRParameterScreenClass: @"Account Setup"
    }];
}



- (IBAction)onSkip:(id)sender
{
    [FIRAnalytics logEventWithName:@"user_interaction"
          parameters:@{
                       @"name": @"Skip Setup"
                       }];
       
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HasLaunchedOnce"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self dismissViewControllerAnimated:YES completion:nil];
}



- (IBAction)onValidation:(id)sender
{
    [FIRAnalytics logEventWithName:@"user_interaction"
        parameters:@{@"name": @"Validate Account"
    }];
    //Did we provide a barcode?
    if ([self.txtBarcode.text length] == 0)
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(@"No Barcode Provided", "")
                                              message:NSLocalizedString(@"You must provide a barcode.", "")
                                              preferredStyle:UIAlertControllerStyleActionSheet];
        
        //Add Cancel Button
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"Close", "")
                                       style:UIAlertActionStyleCancel
                                       handler:nil];
        
        [alertController addAction:cancelAction];
        
        
        //Show to user
       // UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
        //alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
        //alertPopoverPresentationController.sourceView = self.view;
        [self presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
    
    /*
    //Does the library require a password?
    if (self.currentLibrary.requiresPassword)
    {
        if ([self.txtPassword.text length] == 0)
        {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"No Password Provided"
                                                  message:@"You must provide a password."
                                                  preferredStyle:UIAlertControllerStyleActionSheet];
            
            //Add Cancel Button
            UIAlertAction *cancelAction = [UIAlertAction
                                           actionWithTitle:@"Close"
                                           style:UIAlertActionStyleCancel
                                           handler:nil];
            
            [alertController addAction:cancelAction];
            
            
            //Show to user
            UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
            alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
            alertPopoverPresentationController.sourceView = self.view;
            [self presentViewController:alertController animated:YES completion:nil];
            
            return;
        }
    }*/
    
    
    [DejalBezelActivityView activityViewForView:self.view];
    
    [accountManager validateCredentials:self.txtBarcode.text password:self.txtPassword.text];
    
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 80; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

#pragma mark -
#pragma mark WEBSERVICE_DELEGATES

- (void)accountValidated:(CTPatronAccount *)account
{
    
    [DejalBezelActivityView removeViewAnimated:YES];
    
    if (![accountManager accountExistsWithBarcode:account.barcode])
    {
        account.isPrimaryAccount = YES;
        [accountManager addAccount:account];
    }
    
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:NSLocalizedString(@"Success", "")
                                          message:NSLocalizedString(@"Your account is now available for use.", "")
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Close", "")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * _Nonnull action) {
                                       [self dismissViewControllerAnimated:YES completion:^{
                                           if ([self.delegate respondsToSelector:@selector(didUpdateAccounts)])
                                           {
                                               [self.delegate performSelector:@selector(didUpdateAccounts)];
                                           }
                                       }];
                                   }];
    
    
    [alertController addAction:cancelAction];
    
    UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
    alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
    alertPopoverPresentationController.sourceView = self.view;
    
    [self presentViewController:alertController animated:YES completion:^{

    }];
}


- (void) validationFailed:(NSString *)failureMsg
{
    [DejalBezelActivityView removeViewAnimated:YES];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:NSLocalizedString(@"Error", "")
                                          message:failureMsg
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Close", "")
                                   style:UIAlertActionStyleCancel
                                   handler:nil];
    
    
    [alertController addAction:cancelAction];
    
    UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
    alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
    alertPopoverPresentationController.sourceView = self.view;
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)didFailWithError:(NSString *)errorMsg httpStatusCode:(NSInteger)statusCode
{
    [DejalBezelActivityView removeViewAnimated:YES];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:NSLocalizedString(@"Error", "")
                                          message:errorMsg
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Close", "")
                                   style:UIAlertActionStyleCancel
                                   handler:nil];
    
    
    [alertController addAction:cancelAction];
    
    UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
    alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
    alertPopoverPresentationController.sourceView = self.view;
    
    [self presentViewController:alertController animated:YES completion:nil];
}

@end

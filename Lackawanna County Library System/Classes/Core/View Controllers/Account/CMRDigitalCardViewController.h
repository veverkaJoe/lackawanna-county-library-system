//
//  CMRDigitalCardViewController.h
//  CapiraMobile Ready
//
//  Created by Michael on 4/2/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import "CMRViewController.h"
#import <ZXingObjC/ZXingObjC.h>
NS_ASSUME_NONNULL_BEGIN

@interface CMRDigitalCardViewController : CMRViewController

@property (nonatomic) CTPatronAccount *chosenPatronAccount;

@property (nonatomic, weak) IBOutlet UILabel        *lblPatronBarcode;
@property (nonatomic, weak) IBOutlet UILabel        *lblPatronName;
@property (nonatomic, weak) IBOutlet UILabel        *lblPatronExpiration;
@property (nonatomic, weak) IBOutlet UIImageView    *imgBarcode;
@property (nonatomic, weak) IBOutlet UIImageView    *imgLibraryCard;

@end

NS_ASSUME_NONNULL_END

//
//  CMRAccountMenuViewController.m
//  CapiraMobile Ready
//
//  Created by Michael on 3/26/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import "CMRAccountMenuViewController.h"
#import "CMRDigitalCardViewController.h"
#import "CMRManageAccountItemsViewController.h"
#import "CMRSelfCheckoutViewController.h"
@import Firebase;

@interface CMRAccountMenuViewController ()

@end

@implementation CMRAccountMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Set Screen Display Title in Nav Bar
    self.topNavBar.topItem.title = NSLocalizedString(@"Account", "");
    
    if ([[configLibraryInformation objectForKey:@"selfcheckEnabled"] boolValue])
    {
        _btnSelfCheck.hidden = NO;
    }else{
        _btnSelfCheck.backgroundColor = [UIColor whiteColor];
        [_btnSelfCheck removeTarget:nil
                             action:NULL
                   forControlEvents:UIControlEventAllEvents];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [FIRAnalytics logEventWithName:kFIREventScreenView parameters:@{
        kFIRParameterScreenName: @"Account Menu",
        kFIRParameterScreenClass: @"Account Menu"
    }];
}

- (void)checkDeviceAuthorizationStatus {
    
    
}

- (IBAction)showSelfCheckout:(id)sender
{
    [FIRAnalytics logEventWithName:@"user_interaction"
        parameters:@{@"name": @"Show Self Checkout"
    }];
    
    //Checks to see if application has permissions for camera
    [self checkCameraPermissions];
    
//Do we have any accounts?
    if ([accountManager numberOfAccounts] == 0)
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(@"No Accounts Configured", "")
                                              message:NSLocalizedString(@"Please add your library card in My Account > Manage Accounts.", "")
                                              preferredStyle:UIAlertControllerStyleActionSheet];
        
        //Add Cancel Button
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"Close", "")
                                       style:UIAlertActionStyleCancel
                                       handler:nil];
        
        [alertController addAction:cancelAction];
        
        
        //Show to user
        //Show to user
        UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
        alertPopoverPresentationController.sourceRect = ((UIButton *) sender).bounds;
        alertPopoverPresentationController.sourceView = ((UIButton *) sender);
        
        [self presentViewController:alertController animated:YES completion:nil];
        
        return;
        
    }
    
    
    CMRSelfCheckoutViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"Self Checkout"];
    //Do we have only 1 account?
    if ([accountManager numberOfAccounts] == 1)
    {
        vc.chosenPatron = [[accountManager getAllStoredAccounts] objectAtIndex: 0];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        //Prompt to choose account
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(@"Choose Account", "")
                                              message:nil
                                              preferredStyle:UIAlertControllerStyleActionSheet];
        
        for (CTPatronAccount *account in [accountManager getAllStoredAccounts])
        {
            UIAlertAction *action = [UIAlertAction
                                     actionWithTitle:account.name
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction *action)
                                     {
                                         
                                         vc.chosenPatron = [self->accountManager getAccountWithBarcode:account.barcode];
                                         [self.navigationController pushViewController:vc animated:YES];
                                     }];
            
            [alertController addAction:action];
        }
        
        //Add Cancel Button
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"Cancel", "")
                                       style:UIAlertActionStyleCancel
                                       handler:nil];
        
        [alertController addAction:cancelAction];
        
        //Show to user
        UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
        alertPopoverPresentationController.sourceRect = ((UIButton *) sender).bounds;
        alertPopoverPresentationController.sourceView = sender;
        [self presentViewController:alertController animated:YES completion:nil];
    }
}


- (IBAction)showDigitalLibraryCard:(id)sender
{
    [FIRAnalytics logEventWithName:@"user_interaction"
                        parameters:@{@"name": @"Show Digital Library Card"
                        }];
    
    //Do we have any accounts?
    if ([accountManager numberOfAccounts] == 0)
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(@"No Accounts Configured", "")
                                              message:NSLocalizedString(@"Please add your library card in My Account > Manage Accounts.", "")
                                              preferredStyle:UIAlertControllerStyleActionSheet];
        
        //Add Cancel Button
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"Close", "")
                                       style:UIAlertActionStyleCancel
                                       handler:nil];
        
        [alertController addAction:cancelAction];
        
        
        //Show to user
        //Show to user
        UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
        alertPopoverPresentationController.sourceRect = ((UIButton *) sender).bounds;
        alertPopoverPresentationController.sourceView = ((UIButton *) sender);
        
        [self presentViewController:alertController animated:YES completion:nil];
        
        return;
        
    }
    
    CMRDigitalCardViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"Digital Library Card"];
    //Do we have only 1 account?
    if ([accountManager numberOfAccounts] == 1)
    {
        vc.chosenPatronAccount = [[accountManager getAllStoredAccounts] objectAtIndex: 0];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        //Prompt to choose account
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(@"Choose Account", "")
                                              message:nil
                                              preferredStyle:UIAlertControllerStyleActionSheet];
        
        for (CTPatronAccount *account in [accountManager getAllStoredAccounts])
        {
            UIAlertAction *action = [UIAlertAction
                                     actionWithTitle:account.name
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction *action)
                                     {
                vc.chosenPatronAccount = [self->accountManager getAccountWithBarcode:account.barcode];
                [self.navigationController pushViewController:vc animated:YES];
            }];
            
            [alertController addAction:action];
        }
        
        //Add Cancel Button
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"Cancel", "")
                                       style:UIAlertActionStyleCancel
                                       handler:nil];
        
        [alertController addAction:cancelAction];
        
        //Show to user
        UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
        alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
        alertPopoverPresentationController.sourceView = self.view;
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    
}



- (IBAction)showHolds:(id)sender
{
    [FIRAnalytics logEventWithName:@"user_interaction"
                        parameters:@{@"name": @"Show Holds"
                        }];
    //Do we have any accounts?
    if ([accountManager numberOfAccounts] == 0)
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(@"No Accounts Configured", "")
                                              message:NSLocalizedString(@"Please add your library card in My Account > Manage Accounts.", "")
                                              preferredStyle:UIAlertControllerStyleActionSheet];
        
        //Add Cancel Button
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"Close", "")
                                       style:UIAlertActionStyleCancel
                                       handler:nil];
        
        [alertController addAction:cancelAction];
        
        
        //Show to user
        //Show to user
        UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
        alertPopoverPresentationController.sourceRect = ((UIButton *) sender).bounds;
        alertPopoverPresentationController.sourceView = ((UIButton *) sender);
        
        [self presentViewController:alertController animated:YES completion:nil];
        
        return;
        
    }
    
    
    CMRManageAccountItemsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"Manage Account"];
    vc.requestType = kAccountItemsHolds;
    //Do we have only 1 account?
    if ([accountManager numberOfAccounts] == 1)
    {
        vc.chosenPatron = [[accountManager getAllStoredAccounts] objectAtIndex: 0];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        //Prompt to choose account
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(@"Choose Account", "")
                                              message:nil
                                              preferredStyle:UIAlertControllerStyleActionSheet];
        
        for (CTPatronAccount *account in [accountManager getAllStoredAccounts])
        {
            UIAlertAction *action = [UIAlertAction
                                     actionWithTitle:account.name
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction *action)
                                     {
                
                vc.chosenPatron = [self->accountManager getAccountWithBarcode:account.barcode];
                [self.navigationController pushViewController:vc animated:YES];
            }];
            
            [alertController addAction:action];
        }
        
        //Add Cancel Button
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"Cancel", "")
                                       style:UIAlertActionStyleCancel
                                       handler:nil];
        
        [alertController addAction:cancelAction];
        
        //Show to user
        UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
        alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
        alertPopoverPresentationController.sourceView = self.view;
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (IBAction)showCheckouts:(id)sender
{
    [FIRAnalytics logEventWithName:@"user_interaction"
                        parameters:@{@"name": @"Show Checkouts"
                        }];
    //Do we have any accounts?
    if ([accountManager numberOfAccounts] == 0)
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(@"No Accounts Configured", "")
                                              message:NSLocalizedString(@"Please add your library card in My Account > Manage Accounts.", "")
                                              preferredStyle:UIAlertControllerStyleActionSheet];
        
        //Add Cancel Button
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"Close", "")
                                       style:UIAlertActionStyleCancel
                                       handler:nil];
        
        [alertController addAction:cancelAction];
        
        
        //Show to user
        //Show to user
        UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
        alertPopoverPresentationController.sourceRect = ((UIButton *) sender).bounds;
        alertPopoverPresentationController.sourceView = ((UIButton *) sender);
        
        [self presentViewController:alertController animated:YES completion:nil];
        
        return;
        
    }
    
    
    CMRManageAccountItemsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"Manage Account"];
    vc.requestType = kAccountItemsCheckouts;
    //Do we have only 1 account?
    if ([accountManager numberOfAccounts] == 1)
    {
        vc.chosenPatron = [[accountManager getAllStoredAccounts] objectAtIndex: 0];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        //Prompt to choose account
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(@"Choose Account", "")
                                              message:nil
                                              preferredStyle:UIAlertControllerStyleActionSheet];
        
        for (CTPatronAccount *account in [accountManager getAllStoredAccounts])
        {
            UIAlertAction *action = [UIAlertAction
                                     actionWithTitle:account.name
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction *action)
                                     {
                
                vc.chosenPatron = [accountManager getAccountWithBarcode:account.barcode];
                [self.navigationController pushViewController:vc animated:YES];
            }];
            
            [alertController addAction:action];
        }
        
        //Add Cancel Button
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"Cancel", "")
                                       style:UIAlertActionStyleCancel
                                       handler:nil];
        
        [alertController addAction:cancelAction];
        
        //Show to user
        UIPopoverPresentationController *alertPopoverPresentationController = alertController.popoverPresentationController;
        alertPopoverPresentationController.sourceRect = [self centerViewRect:self.view];
        alertPopoverPresentationController.sourceView = self.view;
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

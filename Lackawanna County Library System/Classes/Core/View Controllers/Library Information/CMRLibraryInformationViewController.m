//
//  CMRLibraryInformationViewController.m
//  CapiraMobile Ready
//
//  Created by Michael on 4/2/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import "CMRLibraryInformationViewController.h"
@import Firebase;

@interface CMRLibraryInformationViewController ()

@end

@implementation CMRLibraryInformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Set Screen Display Title in Nav Bar
    self.topNavBar.topItem.title = NSLocalizedString(@"Library Information", "");
    
    //Set address label text
    NSString *addressString = [NSString stringWithFormat:@"%@\n%@", [configLibraryInformation objectForKey:@"name"], [[configLibraryInformation objectForKey:@"addresses"] objectForKey:@"main"]];
    self.lblLibraryAddress.text = addressString;
    
    NSLog(@"%@", configLibraryInformation);
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [FIRAnalytics logEventWithName:kFIREventScreenView parameters:@{
        kFIRParameterScreenName: @"Library Information",
        kFIRParameterScreenClass: @"Library Information"
    }];
}

- (IBAction)showHours:(id)sender
{
    [FIRAnalytics logEventWithName:@"user_interaction"
        parameters:@{@"name": @"Hours"
    }];
    
    [self displayBrowser:[[configLibraryInformation objectForKey:@"websites"] objectForKey:@"hours"]];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

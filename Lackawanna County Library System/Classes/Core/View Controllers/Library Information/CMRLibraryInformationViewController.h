//
//  CMRLibraryInformationViewController.h
//  CapiraMobile Ready
//
//  Created by Michael on 4/2/20.
//  Copyright © 2020 Capira Technologies LLC. All rights reserved.
//

#import "CMRViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CMRLibraryInformationViewController : CMRViewController

@property (nonatomic, weak) IBOutlet UILabel *lblLibraryAddress;
@end

NS_ASSUME_NONNULL_END

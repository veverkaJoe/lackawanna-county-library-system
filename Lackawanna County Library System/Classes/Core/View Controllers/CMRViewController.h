#import <UIKit/UIKit.h>

#import <CTUtilities/CTConfigurationManager.h>
#import <CTAccountsManager/CTAccountsManager.h>
#import <CTUtilities/CTUtilities.h>
#import "CTLibrary.h"
#import "DejalActivityView.h"
#import <MessageUI/MessageUI.h>

@import SafariServices;

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)


@interface CMRViewController : UIViewController <MFMailComposeViewControllerDelegate, UISearchBarDelegate, UIScrollViewDelegate, UINavigationBarDelegate, UITabBarDelegate>
{
    @protected
    NSDictionary *configLibraryInformation;
    
    @protected
    CTAccountManager *accountManager;
}
//Top banner image
@property (nonatomic, weak) IBOutlet UIImageView        *imgBanner;

//Background image, if needed
@property (nonatomic, weak) IBOutlet UIImageView        *imgBackground;

//Top navigation bar
@property (nonatomic, weak) IBOutlet UINavigationBar    *topNavBar;

//Bottom tab bar
@property (nonatomic, weak) IBOutlet UITabBar           *bottomTabBar;

//Scrollview for screens where needed.
@property (nonatomic, weak) IBOutlet UIScrollView       *scrollView;

//Search Bar for quick search
@property (nonatomic, weak) IBOutlet UISearchBar        *searchBar;


- (CGRect)centerViewRect:(UIView *)view;

-(void)displayBrowser:(NSString *)URLString;
-(void)checkCameraPermissions;

@end
